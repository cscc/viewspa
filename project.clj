(defproject edu.unc.cscc/views "0.1.0-SNAPSHOT"
  :description "views front-end"
  :url "https://gitlab.com/cscc/viewspa"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.329"]
                 [org.clojure/core.async "1.5.648"]
                 [cljs-ajax "0.7.3"]
                 [cljs-hash "0.0.2"]
                 [secretary "1.2.3"]
                 ; reagent and related
                 [reagent "0.8.1"]
                 ; state management
                 [re-frame "0.10.5"]
                 ; our belove console
                 [shodan "0.4.2"]]

  :source-paths ["src"]
  :resource-paths ["target", "resources"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :figwheel {:css-dirs ["resources/public/css"]}
  :aliases {"fig" ["trampoline" "run" "-m" "figwheel.main"]}
  :profiles {:dev {:dependencies [[com.bhauman/figwheel-main "0.2.3"]
                                  [org.clojure/tools.nrepl "0.2.13"]
                                  [com.bhauman/rebel-readline-cljs "0.1.4"]
                                  [cider/piggieback "0.3.6"]]
                   :source-paths ["src" "dev"]
                   ;; If you use CIDER you'll want this.
                   ;; :plugins [[cider/cider-nrepl "0.12.0"]]
                   ;:repl-options
                   #_{; for nREPL dev you really need to limit output
                                  :init (set! *print-length* 50)
                                  :nrepl-middleware [cider.piggieback/wrap-cljs-repl]}}})
