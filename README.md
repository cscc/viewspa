viewspa
===

viewspa is the single-page client portion of the views application.


Configuration
---

`viewspa` can be configured via `src/edu/unc/cscc/views/config.cljs`

Building/running
---

Build with leiningen, support for a production build is in progress

For dev builds run `lein fig -- -b dev -r` to load up the app automatically with
hot reloading.

### Setting up Auth0

First create an Auth0 tenant, and create an SPA application within it. You will
use this application for setting up the views frontend.

Once the application is created create the file
`resources/public/js/auth0-config.json` and put the following in:
```
{
    "domain": <auth0 domain>
    "clientId": <auth0 clientId>
}
```

Where `<auth0 domain>` is replaced with the Auth0 domain for your account, and
`<auth0 clientId>` is replaced with the auth0 clientId for your account.

Be sure that the server side views application is set up within the same tenant.

Adding External Libraries
---

External javascript libraries can be added by

1. Downloading the libraries' bundled .js file and putting it in the `external` directory.
2. Go to
   [http://jmmk.github.io/javascript-externs-generator/](http://jmmk.github.io/javascript-externs-generator/)
   and create an extern file for the external module.
3. Create a file called `<library-name>.externs.js` replacing `<libray-name>`
   with the name of the module (ex. `interact.js` gets an extern file called
   `interact.externs.js`)
4. Add an entry in the `:foreign-libs` key of the build files (`dev.cljs.edn`
   and `prod.cljs.edn` at time of writing). The provides key is the JavaScript
   object the library exposes
   For example, `interact`'s looks like:
   ```
   {:file "external/interact.js"
    :provides ["interact"]}
    ```
5. Rebuild and use your new library!

Deploying
----

First update the `edu.unc.cscc.views.config` namespace to have the proper values
for your production server. Also update `resources/public/js/auth0-config.json`
to use production auth0 details. Then build with `lein fig -- -b prod`

Once building has completed copy the files in `resources/public/` to the root of
your webserver, and you're good to go!

Using with Docker
---

A `Dockerfile` is provided which will handle building `viewspa` and creating 
an nginx-based container for serving it.  Build and run in the usual manner
for your preferred deployment scenario.  By default, will serve over port 80
(which may need explicit exposure/routing, depending on configuration.)
