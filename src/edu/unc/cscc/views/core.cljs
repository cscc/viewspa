(ns edu.unc.cscc.views.core
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [secretary.core :as secretary :include-macros true]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [shodan.console :as console]
            ; our stuff
            [edu.unc.cscc.views.config :as config]
            [edu.unc.cscc.views.layout.header :as layout-header]
            [edu.unc.cscc.views.layout.footer :as layout-footer]
            [edu.unc.cscc.views.layout.view-container :as layout-view-container]
            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.auth0 :as auth0]
            [edu.unc.cscc.views.queries :as queries]
            [edu.unc.cscc.views.event-handlers :as event-handlers]
            [edu.unc.cscc.views.remote-api :as remote]
            [edu.unc.cscc.views.utils.urls :as urls]
            [edu.unc.cscc.views.routes :as routes]))

(defn- setup-browser-navigation []
  ; TODO
  )
(secretary/set-config! :prefix "#")


;;;;; hack to prevent reload
(defonce has-inited?
  (r/atom false))


;;;;; base view

(defn top-view
  []
  (let [active (subscribe [:active-view])
        user-id (subscribe [:current-user-id])]
    (fn []
      ; redirect logged-out users
      (if (and
            (not (or (= :login @active) (= :about @active) (= :limbo @active)))
            (not @user-id))
        (routes/go! (routes/login)))
      [:div {:class "container"}
       [layout-header/component]
       [layout-view-container/component @active]
       [layout-footer/component]])))

;;;;; init

(defn- mount
  []
  (r/render
   [top-view]
   (.getElementById js/document "app")))

(defn- successful-auth0-authentication
  [token]
  (dispatch-sync [:set-jwt-token token])
  (remote/user-load
   :current
   (fn [user]
     (event-handlers/user-load-success-handler! user)
     (mount))
   (fn [error]
     (event-handlers/user-load-failure-handler! error)
     (mount))))

(defn- init! []
  (let [query-params (urls/parse-search-params (-> js/window .-location .-search))]
    (when (not @has-inited?)
                                        ; rename our webCharts symbol
      (aset js/window config/web-charts-var-name (.-webCharts js/window))
      (aset js/window "webCharts" nil)
      (dispatch-sync [:initialize])
      (reset! has-inited? true))
    (remote/initialize!)
    (routes/initialize!)
    (when (:loaderror query-params)
      (dispatch-sync [:set-load-error (keyword (:loaderror query-params))]))
    (auth0/do-auth! successful-auth0-authentication mount)))

(init!)

; figwheel hook
(defn js-reload []
  (init!))
