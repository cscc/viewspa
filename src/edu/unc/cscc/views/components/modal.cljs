(ns edu.unc.cscc.views.components.modal
  (:require [reagent.core :as reagent]))

(def close
  [:svg {:xmlns "http://www.w3.org/2000/svg", :viewBox "0 0 48 48" :width "20"}
   [:path {:d "m12.45 37.65-2.1-2.1L21.9 24 10.35 12.45l2.1-2.1L24 21.9l11.55-11.55 2.1 2.1L26.1 24l11.55 11.55-2.1 2.1L24 26.1Z"}]])

(defn modal
  "Creates a modal. Expects the options map to include a :open key for when the modal
  should be open and an :onClose key which is called when the modal should be closed"
  ([optionsIn & childrenIn]
   (let [children (if (map? optionsIn) childrenIn (cons optionsIn childrenIn))
         {:keys [open onClose]} (if (map? optionsIn) optionsIn {})]
     (when open
       [:<>
        [:div.overlay]
        [:div.modal
         [:div.modal-header
          [:button.button.icon-button {:onClick onClose} close]]
         [:div.modal-content
          children]]]))))
