(ns edu.unc.cscc.views.config)

; URL base for all API calls to the backend
; dev
(def endpoint "http://localhost:8080")
; prod
;(def endpoint "https://views.example.com:8443")

;dev
(def homepage "/index-dev.html")
;prod
;(def homepage "/")

; used in the header or really anywhere that we need to refer to ourselves
(def product-name "views")

; user-facing version.  May or may not correspond to tags
(def product-version "0.1")

; ID of local (to views) credential source
(def local-credential-source-id "local")

; ID of Auth0 (default) credential source
(def auth0-credential-source-id "auth0")

; symbol that webCharts gets renamed to
(def web-charts-var-name (str "__views__webCharts_" (.getTime (js/Date.))))

;; These users' auth source can't be changed
(def protected-users ["admin", "drupgod"])
