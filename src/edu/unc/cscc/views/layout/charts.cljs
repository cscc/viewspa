(ns edu.unc.cscc.views.layout.charts
  (:require [clojure.string :as string]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console :include-macros true]
            [cljs-hash.md5 :refer [md5]]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.layout.nav :as nav]
            [edu.unc.cscc.views.routes :as routes]
            [edu.unc.cscc.views.layout.edit-chart :as edit-chart]
            [edu.unc.cscc.views.layout.pager :refer [pager]]
            [edu.unc.cscc.views.layout.view-chart :as view-chart]))

(defn- djb2
  [s]
  (reduce
    (fn [h c] (+ (+ (bit-shift-left h 5) h) (int c)))
    5381
    (seq s)))

(defn- color->css
  [{:keys [red green blue]}]
  (str "rgba(" red ", " green "," blue ", 0.8)"))

(defn- generate-color
  [project]
  (let [hash (djb2 (md5 (:name project)))
        r (bit-shift-right (bit-and hash 0xFF0000) 16)
        g (bit-shift-right (bit-and hash 0x00FF00) 8)
        b (bit-and hash 0x0000FF)]
    {:red r :green g :blue b}))

(defn- yiq
  [{:keys [red green blue]}]
  (/ (+ (* red 299) (* green 587) (* blue 114)) 1000))

(defn- generate-contrast
  [color]
  (if (< 128 (yiq color)) {:red 0 :green 0 :blue 0} {:red 255 :green 255 :blue 255}))

(defn- chart-entry
  [_ _ _]
  (fn [chart can-modify? on-select]
    (assert on-select "missing on-select")
    (let [chart-color (generate-color chart)]
      [:div
        {:class "four columns noselect"
          :style {:cursor :pointer}
          :on-click #(on-select chart)}
        ; icon
        [:div
          {:style
            {:width "100%" :height "10rem"
              :color (color->css (generate-contrast chart-color))
              :background-color (color->css chart-color)
              :outline "1px #ccc solid"

              :text-align :center
              :display :table
              :position :relative}}
          [:div
            {:style {:display :table-cell :vertical-align :middle}}
            [:span
              {:class "no-only-mobile" :style
                {:font-size "6rem"
                  :line-height "6rem"}}
              (string/upper-case (get (:name chart) 0))]]
          [:div
            {:style
              {:display :table-cell
                :vertical-align :middle}}
            [:span
              {:class "yes-mobile"
                :style
                {:font-size "4rem"
                  :line-height "4rem"
                  :padding "1rem"}}
              (:name chart)]]
          (when can-modify?
            [:button
              {:class (if (< 128 (yiq chart-color)) "black-bg" "white-bg") :style
                {:position :absolute
                  :top 0
                  :right 0
                  :margin 0
                  :padding "0 0.25rem"
                  :border 0
                  :font-size "1.6rem"
                  :font-weight :bold
                  :color :inherit}
                :on-click
                  (fn [e]
                    (.preventDefault e)
                    (.stopPropagation e)
                    (routes/go! (routes/chart {:id (:id chart) :project-id (:id (:project chart)) :query-params {:action "edit"}}))
                    false)}
              "☰"])]
        [:h6
          {:class "no-mobile"
            :style
            {:margin 0
              :word-wrap :all
              :word-break :break-word}}
          (:name chart)]
        [:div
          {:style
            {:margin "0.25rem 0"
              :padding "0 0.25rem"
              :font-size "10pt"
              :line-height 1.2}}
          (:description chart)] ])))

(defn chart-list
  [_]
  (let [project-id (subscribe [:view-state :project-id])
        project-acl (subscribe [:projects/acl] [project-id])
        search-term (r/atom "")
        page (r/atom 1)
        page-size 9
        user (subscribe [:current-user])
        params (r/atom {:term "" :project-id @project-id :with-user-acl? true :limit page-size :offset 0})
        results (subscribe [:charts/search] [params])
        user (subscribe [:current-user])]
    (fn [{:keys [with-new-button? allow-edit? filter-fn on-select]
          :or {with-new-button? true
              filter-fn (fn [] true)
              allow-edit? false
              on-select
                (fn [chart]
                  (routes/go!
                    (routes/chart {:id (:id chart) :project-id (:id (:project chart))})))}}]
      (let [charts (filter filter-fn (:results @results))
            acls (:acls @results)
            allow-create-new? (and (auth/acl-allows? (:id @user) @project-acl :create-charts) with-new-button?)
            exec-search
              (fn [term]
                (reset! page 1)
                (swap! params assoc :term term :offset 0))]
        [:section {:class "twelve columns"
                   :id "chart-listing-container"}
          (if (= :loading @project-acl)
            [:p "Loading..."]
            [:div
              [:section {:class "row"}
                (when allow-create-new?
                  [:section {:style {:float :left}}
                    [:button
                      {:style {:margin-left 0}
                        :on-click #(routes/go! (routes/chart {:id "new" :project-id @project-id}))}
                      "new chart"]])
                [:section {:id "chart-search-container" :class "text-right"}
                 [:input {:type "search"
                          :placeholder "search"
                          :value @search-term
                          :name "chart-search"
                          :id "chart-search-field"
                          :on-key-down
                          (fn [e]
                            (when (= 13 (.-keyCode e))
                              (exec-search @search-term)))
                          :on-change #(reset! search-term (-> % .-target .-value))}]
                 [:button
                   {:on-click #(exec-search @search-term)
                    :disabled (if (= (:term @params) @search-term) :disabled nil)
                    :style {:margin-left "1rem"}} "search"]]]
              [:section {:style {:margin-top "1rem"}}
                (if (= :loading @results)
                  [:section {:class "row"} "Loading charts..."]
                  (if (= :error @results)
                    [:section {:class "row"} "Error loading charts!"]
                    [:section {:class "row"}
                      (if (empty? charts)
                        [:div "No charts found. "
                          (when allow-create-new?
                            [:span "Would you like to "
                              [:a {:href (routes/chart {:id "new" :project-id @project-id})}
                                "create one?"]])]
                        [:section {:class "row twelve columns"}
                          (let [u @user]
                            (for [row (partition 3 3 nil charts)]
                              ^{:key row}
                              [:section {:class "row"}
                                (for [chart row]
                                  ^{:key chart}
                                  [chart-entry chart
                                    (and allow-edit?
                                      (auth/acl-allows? (:id u) (get acls (:id chart)) :write))
                                    on-select])]))])]))]]) 
            [:div {:style {:margin "0 auto" :padding 0 :text-align :center}} 
              [pager
                {:page-size page-size :item-count (:total-count @results) :start-page @page 
                  :loading? (= :loading @results)
                  :page-fn
                    (fn [new-page]
                      (reset! page new-page)
                      (swap! params assoc :offset (* (dec new-page) page-size))) }]]]))))

(defn component
  []
  (let [view-state (subscribe [:view-state])]
    (fn []
      (let [chart-id (:id @view-state)
            mode (:mode @view-state)]
        (if (nil? chart-id)
          [:div {:class "eight columns"} [chart-list {:with-new-button? true :allow-edit? true}]]
          [:section {:class (when (not (= :edit-script mode)) "eight columns")}
           (condp = mode
             :edit [edit-chart/component chart-id]
             :edit-script [edit-chart/script-component chart-id]
             [view-chart/component chart-id])])))))
