(ns edu.unc.cscc.views.layout.dashboards
  (:require
    [reagent.core :as r]
    [re-frame.core :refer [subscribe dispatch]]
    [shodan.console :as console]
    [cljs-hash.md5 :refer [md5]]
    [clojure.string :as string]

    [edu.unc.cscc.views.routes :as routes]
    [edu.unc.cscc.views.layout.charts :as charts]
    [edu.unc.cscc.views.layout.controls :as controls]
    [edu.unc.cscc.views.remote-api :as remote]
    [edu.unc.cscc.views.layout.pager :refer [pager]]
    [edu.unc.cscc.views.layout.view-chart :as view-chart]
    ))

(defn- add-chart
  [on-add-chart existing-chart-ids]
  (fn [on-add-chart existing-chart-ids]
    [charts/chart-list
      {:with-new-button? false :on-select on-add-chart
        :filter-fn (fn [chart] (not (some #{(:id chart)} existing-chart-ids)))}]))

(defn- dashboard-grid
  [_]
  (let []
    (fn [{:keys [dashboard allow-edit? on-add-chart on-remove-chart width]
          :or {allow-edit? false width 2}}]
      (let [charts (concat (:charts dashboard) (if allow-edit? [:add]))
            rows (partition width width nil charts)
            existing-chart-ids (map #(:id %) (:charts dashboard))
            chart-width
            (condp = width
              1 "twelve"
              3 "four"
              "six")]
        [:div
          (for [row rows]
            ^{:key [row chart-width]}
            [:div
              {:class "row" :style {:margin-bottom "1rem"}}
              (for [chart row]
                ^{:key [chart chart-width]}
                [:div
                  {:class (str "chart " chart-width " columns")
                    :style
                      (if (not (= chart (last row)) )
                        {:border-right "0.1rem #ccc solid" :margin-right "-0.1rem"})}
                  (if (= :add chart)
                    [add-chart on-add-chart existing-chart-ids]
                    [:div
                      (when allow-edit? [:button
                        {:style {:float :right}
                          :on-click #(on-remove-chart chart)}
                        "X"])
                      [view-chart/component (:id chart)]])])])]))))

(defn- dashboard-component
  [db edit?]
  (let [dashboard (r/atom db)
        taken-names (r/atom [])
        new? (not (number? (:id @dashboard)))
        show-nav? (subscribe [:view-state :show-nav])
        project-id (subscribe [:view-state :project-id])
        error (r/atom "")
        saving? (r/atom false)
        can-delete? (r/atom false)
        can-edit? (r/atom true); TODO - acl support
        width (subscribe [:view-state :grid-width])
        ]
    (fn [_ edit?]
      [:div {:id "new-entity"
              :class "row twelve columns"}
        [:fieldset {:style {:margin 0}}
          [:div {:class "clearfix" :style {:margin-bottom "1rem"}}
            (if edit?
              [:label {:style {:display :inline-block :width "50%" :float :left}}
                [:input
                  {:style {:font-size "1.5rem"}
                    :type :text
                    :placeholder "dashboard name"
                    :on-change
                      (fn [e]
                        (swap! dashboard assoc :name (-> e .-target .-value)))
                    :value (:name @dashboard)}]]
              [:div
                [:div {:class "clearfix"}
                  [:h4 {:style {:margin 0 :float :left}} (:name @dashboard)]
                  (when can-edit?
                    [:button
                      {:style {:float :right}
                        :on-click
                          #(routes/go!
                            (routes/dashboard
                              {:id (:id @dashboard)
                                :project-id @project-id
                                :query-params {:action "edit"}}))}
                      "edit"])]])

            ; buttons
            (when edit?
              [:div {:style {:width "50%" :float :right}}
                [:button
                  {:disabled (or @saving? (empty? (:name @dashboard)))
                    :on-click
                    (fn []
                      (swap! saving? not)
                      (remote/dashboard-save
                        {:dashboard @dashboard
                          :project-id @project-id
                          :on-success
                          (fn [id]
                            (console/log "Saved!")
                            (reset! error nil)
                            (reset! saving? false)
                            (routes/go! (routes/dashboard {:project-id @project-id :id id}))
                            (swap! dashboard assoc :id id))
                          :on-failure
                            (fn [msg]
                              (reset! error (str "save failed: " msg))
                              (reset! saving? false))
                          :on-duplicate-name
                            (fn [name]
                              (swap! taken-names conj name)
                              (reset! saving? false))}))
                    :style {:float :right :margin-left "0.5rem" :margin-bottom "0.5rem"}}
                  (if @saving? "saving..." "save")]
                [:button
                  {:disabled @saving?
                    :on-click #(routes/go! (routes/dashboards {:project-id @project-id}))
                    :style {:float :right}
                    :class "no-mobile-hd"}
                  "cancel"]
                [:button
                  {:disabled @saving?
                    :on-click #(routes/go! (routes/dashboards {:project-id @project-id}))
                    :style {:float :right}
                    :class "yes-mobile-hd"}
                  "<<"]])
            (when edit?
              [:p
                {:class "error"
                :style
                  {:text-align :right
                    :margin-top "1rem"
                    :height "2rem"}}
                (if @error
                  @error
                  (if (some #(= (:name @dashboard) %) @taken-names) "name already in use"))])]
          (if edit?
            [:label {:class ""}
              [:textarea {:style {:min-height "2rem"}
                            :placeholder
                            "A friendly description of the dashboard. Or not.  It's your call."
                            :value (:description @dashboard)
                            :on-change #(swap! dashboard assoc :description (-> % .-target .-value))}]]
            [:p {:style {:margin 0}} (:description @dashboard)])
          (when (and edit? (not new?))
            [:div
              [:button
                {:style (merge (if @can-delete? {:background :red :color :white})
                          {:margin-bottom "0.5rem"})
                  :on-click
                  (fn []
                    (if @can-delete?
                      (remote/dashboard-delete
                        {:id (:id @dashboard)
                          :on-success #(routes/go! (routes/dashboards {:project-id @project-id}))
                          :on-failure
                          (fn [err]
                            (reset! error (str "delete failed: " err)))})
                      (swap! can-delete? not)))}
                (if @can-delete? "! DELETE !" "delete")]])]
        [:div {:class "clearfix" :style {:text-align :right :margin-bottom "1rem"}}
          ; HACK HACK HACK - show-nav should be handled as a distinct event
          (when (not edit?)
            [:div {:style {:float :right :margin-left "0.5rem"}}
              [controls/toggle-bar
                {:name "show navigation"
                  :on-select #(dispatch [:set [:view :state :show-nav] true])
                  :selected? @show-nav?}
                {:name "hide navigation"
                  :on-select #(dispatch [:set [:view :state :show-nav] false])
                  :selected? (not @show-nav?)}]])
          [:span {:style {:margin-right "0.5rem"}} "grid size"]
          [:div
            {:style {:float :right}}
            (if edit?
              [controls/toggle-bar
                {:name "1"
                  :on-select #(dispatch [:set [:view :state :grid-width] 1])
                  :selected? (= @width 1)}
                {:name "2"
                  :on-select #(dispatch [:set [:view :state :grid-width] 2])
                  :selected? (= @width 2)}]
              [controls/toggle-bar
                {:name "1"
                  :on-select #(dispatch [:set [:view :state :grid-width] 1])
                  :selected? (= @width 1)}
                {:name "2"
                  :on-select #(dispatch [:set [:view :state :grid-width] 2])
                  :selected? (= @width 2)}
                {:name "3"
                  :on-select #(dispatch [:set [:view :state :grid-width] 3])
                  :selected? (= @width 3)}])]]
        (when edit? [:hr {:style {:margin-top "1rem" :margin-bottom "1rem"}}])

        [dashboard-grid
          {:dashboard @dashboard
            :width @width
            :allow-edit? (and edit? can-edit?)
            :on-add-chart #(swap! dashboard assoc :charts (concat (:charts @dashboard) [%]))
            :on-remove-chart
            (fn [chart]
              (swap! dashboard assoc :charts
                (filter #(not (= (:id chart) (:id %))) (:charts @dashboard)))) }]]
      )))

(defn view
  [id edit?]
  (let [dashboard (if (number? id) (subscribe [:dashboards id]) (r/atom {}))]
    (fn [id edit?]
      [:div
        (if (= :loading @dashboard)
          [:p "Loading..."]
          [dashboard-component @dashboard edit?])])))


(defn- djb2
  [s]
  (reduce
    (fn [h c] (+ (+ (bit-shift-left h 5) h) (int c)))
    5381
    (seq s)))

(defn- color->css
  [{:keys [red green blue]}]
  (str "rgba(" red ", " green "," blue ", 0.8)"))

(defn- generate-color
  [dashboard]
  (let [hash (djb2 (md5 (:name dashboard)))
        r (bit-shift-right (bit-and hash 0xFF0000) 16)
        g (bit-shift-right (bit-and hash 0x00FF00) 8)
        b (bit-and hash 0x0000FF)]
    {:red r :green g :blue b}))

(defn- yiq
  [{:keys [red green blue]}]
  (/ (+ (* red 299) (* green 587) (* blue 114)) 1000))

(defn- generate-contrast
  [color]
  (if (< 128 (yiq color)) {:red 0 :green 0 :blue 0} {:red 255 :green 255 :blue 255}))

(defn- dashboard-entry
  [_ _]
  (fn [dashboard can-modify?]
    (let [dashboard-color (generate-color dashboard)
          project-id (subscribe [:view-state :project-id])]
      [:div
        {:class "four columns noselect"
          :style {:cursor :pointer}
          :on-click
            (fn []
              (routes/go!
                (routes/dashboard
                  {:project-id @project-id
                    :id (:id dashboard)})))}
        ; icon
        [:div
          {:style
            {:width "100%" :height "10rem"
              :color (color->css (generate-contrast dashboard-color))
              :background-color (color->css dashboard-color)
              :outline "1px #ccc solid"

              :text-align :center
              :display :table
              :position :relative}}
          [:div
            {:style {:display :table-cell :vertical-align :middle}}
            [:span
              {:class "no-only-mobile" :style
                {:font-size "6rem"
                  :line-height "6rem"}}
              (string/upper-case (get (:name dashboard) 0))]]
          [:div
            {:style
              {:display :table-cell
                :vertical-align :middle}}
            [:span
              {:class "yes-mobile"
                :style
                {:font-size "4rem"
                  :line-height "4rem"
                  :padding "1rem"}}
              (:name dashboard)]]
          (when can-modify?
            [:button
              {:class (if (< 128 (yiq dashboard-color)) "black-bg" "white-bg") :style
                {:position :absolute
                  :top 0
                  :right 0
                  :margin 0
                  :padding "0 0.25rem"
                  :border 0
                  :font-size "1.6rem"
                  :font-weight :bold
                  :color :inherit}
                :on-click
                  (fn [e]
                    (.preventDefault e)
                    (.stopPropagation e)
                    (routes/go!
                      (routes/dashboard
                        {:project-id @project-id
                          :id (:id dashboard)
                          :query-params {:action "edit"}}))
                    false)}
              "☰"])]
        [:h5 {:class "no-mobile" :style {:margin 0}} (:name dashboard)]
        [:div
          {:style
            {:margin "0.25rem 0"
              :padding "0 0.25rem"
              :font-size "10pt"
              :line-height 1.2}}
          (:description dashboard)] ])))

(defn listing
  []
  (let [term (r/atom "")
        project-id (subscribe [:view-state :project-id])
        page-size 9
        page (r/atom 1)
        params (r/atom {:project-id @project-id :term "" 
                          :with-user-acl? true :limit page-size :offset 0})
        user (subscribe [:current-user])
        dashboards (subscribe [:dashboards/search] [params])
        exec-search
          (fn [term]
            (reset! page 1)
            (swap! params assoc :term term :offset 0))]
    (fn []
      [:div {:class "twelve columns"}
        ; search bar
        [:div {:class "text-right" :style {:margin-bottom "1rem"}}
          (when true ; TODO
            [:section {:style {:float :left}}
              [:button
                {:class "no-mobile-hd"
                  :style {:margin-left 0}
                  :on-click #(routes/go! (routes/dashboard {:project-id @project-id :id "new"}))}
                "new dashboard"]
              [:button
                {:class "yes-mobile-hd"
                  :style {:margin-left 0}
                  :on-click #(routes/go! (routes/dashboard {:project-id @project-id :id "new"}))}
                "new"]])
          [:input
            {:type :search :placeholder "search"
              :on-change #(reset! term (-> % .-target .-value))
              :on-key-down
              (fn [e]
                (when (= 13 (.-keyCode e))
                  (exec-search @term)))}]
          [:button
            {:class "no-mobile-hd"
              :style {:margin-right 0}
              :on-click #(exec-search @term)
              :disabled (if (= @term (:term @params)) :disabled)}
            "search"]
          [:button
            {:class "yes-mobile-hd"
              :style {:margin-right 0}
              :on-click #(swap! params assoc :term @term)
              :disabled (if (= @term (:term @params)) :disabled)}
            "🔎"]]
        [:div {:class "row"}
          (if (= :error @dashboards)
            [:p {:class "error"} "Error loading dashboards"]
            (if (= :loading @dashboards)
              [:p "Loading..."]
              (if (or (not (map? @dashboards))
                    (empty? (:results @dashboards)))
                [:p "No dashboards found. "
                  (when true ; FIXME
                    [:span "Would you like to "
                      [:a {:href (routes/dashboard {:project-id @project-id :id "new"})} "create one?"]])]
                (into [:div {:class "twelve columns"}]
                  (for [row-contents (partition 3 3 nil (:results @dashboards))]
                    ^{:key row-contents}
                    (into
                      [:div {:class "row"}]
                      (for [dashboard row-contents]
                        ^{:key dashboard}
                        (let [acl (get (:acls @dashboards) (:id dashboard))]
                          [dashboard-entry dashboard
                          true ;(auth/acl-allows? (:id @user) acl :write)

                          ]))))))))]
        [:hr {:style {:margin 0 :margin-top "1rem" :margin-bottom 0 :width "100%"}}]
        [:div {:style {:margin "0 auto" :padding 0 :text-align :center}} 
              [pager
                {:page-size page-size :item-count (:total-count @dashboards) :start-page @page 
                  :loading? (= :loading @dashboards)
                  :page-fn
                    (fn [new-page]
                      (reset! page new-page)
                      (swap! params assoc :offset (* (dec new-page) page-size))) }]] ])))

(defn component
  []
  (let [vs (subscribe [:view-state])
        user (subscribe [:current-user])]
    (fn []
      ^{:key @vs}
      [:div {:class (if (get @vs :show-nav true) "eight columns" "twelve columns")}
        (if (= :loading @user)
          [:p "Loading..."]
          (if (:id @vs)
            [view (:id @vs) (= :edit (:mode @vs))]
            [listing]))])))
