(ns edu.unc.cscc.views.layout.users
  (:require
    [re-frame.core :refer [subscribe dispatch]]
    [reagent.core :as r]
    [shodan.console :as console]

    [edu.unc.cscc.views.auth :as auth]
    [edu.unc.cscc.views.layout.controls :as c]
    [edu.unc.cscc.views.config :as config]
    [edu.unc.cscc.views.layout.nav :as nav]
    [edu.unc.cscc.views.layout.pager :refer [pager]]
    [edu.unc.cscc.views.remote-api :as remote]
    [edu.unc.cscc.views.components.modal :as modal]
    [edu.unc.cscc.views.routes :as routes]))

(defn- valid-password
  [user]
  (let [password (:password user)]
    (or
      (and (:id user) (nil? password) (empty? password))
      (and
        (not (= (:username user) password))
        (<= 6 (count password))))))

(defn- valid-email?
  [email]
  (when email (re-matches #"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" email)))

(defn- edit-user-component
  [u]
  (let [current-user (subscribe [:current-user])
        new-user (not (:id u))
        user (r/atom u)
        password-visible? (r/atom false)
        error (r/atom nil)
        auth0-error (r/atom nil)
        taken-usernames []
        submitted? (r/atom false)
        auth0-submitted? (r/atom false)
        can-delete? (r/atom false)
        confirm-modal-open (r/atom false)
        auth0-email (r/atom "")]
    (fn [u]
      [:div {:class "row eight columns"}
          [:div
            [:section {:class "row"}
              [:h3 {:class "six columns" :style {:margin-bottom "0.25rem"}}
                (if (:id @user)
                  (:username @user)
                  [:input
                    {:style {:width "100%"}
                      :type :email
                      :placeholder "email"
                      :value (:username @user)
                      :on-change #(swap! user assoc :username (-> % .-target .-value))}])]
              [:div
                {:class "three columns"}
                (when (and (not new-user) (not= (:id @user) (:id @current-user)))
                  [:div
                    [c/toggle-bar
                      {:name "active" :selected? (:active @user) :on-select #(swap! user assoc :active true)}
                      {:name "inactive" :selected? (not (:active @user)) :on-select #(swap! user assoc :active false)}]])]
              [:span
                {:class "three columns"
                  :style {:font-size "10pt" :color "grey" :display (if (:id @user) :block :none)}}
                (if (or (nil? (:credential-source-id @user))
                      (= config/local-credential-source-id (:credential-source-id @user)))
                  [:span "This user is unique to " config/product-name "."]
                  [:span "Authenticated via: " [:br] (:credential-source-id @user)])]]
            [:section {:class "row"}
              [:label {:class "six columns"}
                [:span { :class "prompt" :style {:margin-right "1rem"}} "full name"]
                [:input
                  {:style {:width "100%"}
                    :type :text
                    :value (:name @user)
                    :placeholder "user's full name (optional)"
                    :on-change (fn [e] (swap! user assoc :name (-> e .-target .-value)))}]]
              ; only local users can change passwords
              (if (= config/local-credential-source-id (:credential-source-id @user))
                [:label {:class "six columns"}
                  [:span {:class "prompt" :style {:margin-right "1rem"}} "password (> 6 characters)"]
                  [:input
                    {:style {:width "100%"}
                      :type (if @password-visible? :text :password)
                      :placeholder (when (:id @user) "leave blank to keep existing")
                      :value (:password @user)
                      :on-change
                        (fn [e]
                          (let [p (-> e .-target .-value)]
                            (if (empty? p)
                              (swap! user dissoc :password)
                              (swap! user assoc :password p))))
                      :on-focus #(reset! password-visible? true)
                      :on-blur #(reset! password-visible? false)}]])]
           (when new-user
             [:section {:class "row"}
              [:p "After account creation users will receive an invitation to the provided email address to create an account with Auth0, which they can use to log in."]])
            [:section
              {:style {:margin-top "1rem"}}
              (when (auth/can-manage-users? @current-user)
                [:section
                  {:class "roles"}
                  [:section
                    {:class "row"}
                    [:div {:class "six columns"}
                      [:div
                        "Can this user manage other users and grant/deny them "
                        "permissions (for charts and datasets)?"]
                      [c/toggle-bar
                        {:name "yes" :selected? (auth/can-manage-users? @user)
                          :on-select #(swap! user auth/set-manage-users true)}
                        {:name "no" :selected? (not (auth/can-manage-users? @user))
                          :on-select #(swap! user auth/set-manage-users false)}]]
                    [:div {:class "six columns"}
                      [:div "Can this user modify projects' permissions?"]
                      [c/toggle-bar
                        {:name "yes" :selected? (auth/can-manage-project-permissions? @user)
                          :on-select #(swap! user auth/set-manage-project-permissions true)}
                        {:name "no" :selected? (not (auth/can-manage-project-permissions? @user))
                          :on-select #(swap! user auth/set-manage-project-permissions false)}]]]
                  [:section {:class "row"}
                    [:div {:class "six columns"}
                      [:div "Can this create projects?"]
                      [c/toggle-bar
                        {:name "yes" :selected? (auth/can-create-projects? @user)
                          :on-select #(swap! user auth/set-create-projects true)}
                        {:name "no" :selected? (not (auth/can-create-projects? @user))
                          :on-select #(swap! user auth/set-create-projects false)}]]]])
              [:section
                {:class "row error text-right"}
                [:p @error]]
              [:section {:class "row"}
                [:div {:class "four columns"}
                  (when
                    (and (not (nil? (:id @user)))
                      (not (= (:id @user) (:id @current-user))))
                    [:button
                      {:style
                        (merge {:margin 0}
                          (when @can-delete? {:color :white :background-color :red}))
                        :on-click
                        (fn []
                          (if (not @can-delete?)
                            (swap! can-delete? not)
                            (remote/user-delete
                              {:id (:id @user)
                                :on-success
                                  (fn []
                                    ; if the user can't edit other users, then bounce them home
                                    (if (auth/can-manage-users? @current-user)
                                      (routes/go! (routes/users))
                                      (routes/go! (routes/main))))
                                :on-failure
                                  (fn []
                                    (reset! error "Failed to delete user"))})))}
                      (if @can-delete? "DELETE" "Delete")])]
                [:div {:class "eight columns text-right"}
                  [:button
                    {:on-click
                      (fn []
                        ; if the user can't edit other users, then bounce them home
                        (if (auth/can-manage-users? @current-user)
                          (routes/go! (routes/users))
                          (routes/go! (routes/main))))}
                    "cancel"]
                  [:button
                    {:style {:margin-right 0}
                      :disabled
                        (when
                          (or
                           (and (= config/local-credential-source-id (:credential-source-id @user))
                                (not (valid-password @user)))
                           (and (= config/auth0-credential-source-id (:credential-source-id @user))
                                (not (valid-email? (:username @user))))
                           (nil? (:username @user))
                            @submitted?)
                          :disabled)
                      :on-click
                        (fn []
                          (reset! submitted? true)
                          (remote/user-save
                            @user
                            {:on-success
                              (fn []
                                ; if they edited themselves, update our state
                                (if (= (:id @current-user) (:id @user))
                                  (dispatch [:set [:user :entity] @user]))
                                ; if the user can't edit other users, then bounce them home
                                (if (auth/can-manage-users? @current-user)
                                  (routes/go! (routes/users))
                                  (routes/go! (routes/main))))
                              :on-duplicate-username
                              (fn [username]
                                (reset! submitted? false)
                                (swap! taken-usernames conj username))
                              :on-failure
                              #(do
                                (reset! submitted? false)
                                (reset! error "Failed to save user"))}))}
                    (if @submitted? "saving..." "save")]]]]]
        [:hr {:class "twelve columns"
              :style {:marginTop "1.2rem", :marginBottom "1.2rem"}}]
       (when (and (auth/can-manage-users? @current-user)
                  (not new-user)
                  (not= config/auth0-credential-source-id (:credential-source-id @user))
                  (not (some #{(:username @user)} config/protected-users)))
         [:div
          [:h4 "Move User to Auth0"]
          [:p "WARNING! This will change the user's username to this email address,
             and they will be required to sign in with Auth0 from here on out."]
          [:label {:class "six columns"}
           [:span { :class "prompt" :style {:margin-right "1rem"}} "Auth0 Email"]
           [:input
            {:style {:width "100%"}
             :type :text
             :value @auth0-email
             :placeholder "The email for the user to log into auth0 with"
             :on-change (fn [e] (reset! auth0-email (-> e .-target .-value)))}]]
          [:section
           {:class "row error text-right"}
           [:p @auth0-error]]
          [:div {:class "row eight-columns text-right"}
           [:button
            {:on-click
             (fn []
               ; if the user can't edit other users, then bounce them home
               (if (auth/can-manage-users? @current-user)
                 (routes/go! (routes/users))
                 (routes/go! (routes/main))))}
            "cancel"]
           [:button {:style {:margin-right 0}
                     :disabled (or @auth0-submitted?
                                   (not (valid-email? @auth0-email)))
                     :onClick #(reset! confirm-modal-open true)}
            (if @auth0-submitted? "Updating User..." "Update User")]]
          [:hr {:class "twelve-columns"
                :style {:margin-top "1rem"}}]
          [modal/modal
           {:open @confirm-modal-open :onClose #(reset! confirm-modal-open false)}
           [:<>
            [:p "Are you sure you want to transfer this user to auth0? This is irreversible"]
            [:div.twelve-columns.text-right
             [:button.button {:onClick #(reset! confirm-modal-open false)} "cancel"]
             [:button.button {:onClick
                              (fn []
                                (reset! confirm-modal-open false)
                                (reset! auth0-submitted? true)
                                (remote/user-change-auth-source
                                 @user
                                 @auth0-email
                                 (fn [user]
                                   (reset! auth0-submitted? false)
                                   (when (= (:id @current-user) (:id user))
                                     (dispatch [:set [:user :entity] user]))
                                   (if (auth/can-manage-users? @current-user)
                                     (routes/go! (routes/users))
                                     (routes/go! (routes/main))))
                                 (fn [status]
                                   (reset! auth0-submitted? false)
                                   (if (= 409 status)
                                     (reset! auth0-error "A different user in views has this email")
                                     (reset! auth0-error "Failed to update user")))))}
              "confirm"]]]
           ]])])))

(defn- edit-user
  [user-id]
  (let [user (subscribe [:users user-id])]
    (fn [user-id]
      [:div
        (if (= :loading @user)
          [:p "Loading..."]
          [edit-user-component @user])])))

(defn- new-user
  []
  (fn []
    [:div
      [edit-user-component
        {:username nil :name nil :active true
          :credential-source-id config/auth0-credential-source-id}]]))

(defn listing
  []
  (let [term (r/atom "")
        page-size 20
        page (r/atom 1)
        search-params (r/atom {:term "" :limit page-size :offset 0})
        results (subscribe [:users/search] [search-params])]
    (fn []
      [:section {:class "row eight columns"}
        [:section {:class "row"}
          [:section {:class "four columns"}
            [:button
              {:style {:margin-left 0}
                :on-click #(routes/go! (routes/user {:id "new"}))}
              "new user"]]
          (let [exec-search 
                  (fn [term]
                    (reset! page 1)
                    (swap! search-params assoc :term term :offset 0))]
            [:section {:class "eight columns text-right"}
              [:input {:type "search"
                        :placeholder "search"
                        :value @term
                        :on-key-down
                        (fn [e]
                          (when (= 13 (.-keyCode e))
                            (exec-search @term)))
                        :on-change #(reset! term (-> % .-target .-value))}]
              [:button
                {:on-click #(exec-search @term)
                  :disabled (if (= (:term @search-params) @term) :disabled nil)
                  :style {:margin 0 :margin-left "1rem"}} "search"]])]
        [:section {:class "row"}
          (if (= :loading (:results @results))
            [:div "Loading..."]
            (if (empty? (:results @results))
              [:div
                {:class "twelve columns"
                  :style {:margin-top "3rem"}}
                [:p "No users found.  Want to "
                  [:a {:href "javascript:void(0);"
                        :on-click
                          #(do (reset! term "")
                            (swap! search-params assoc :term @term))}
                      "try again?"]]]
              [:table {:class "twelve columns" :style {:border-collapse :initial}}
                [:thead
                  [:tr
                    [:th "username"]
                    [:th "name"]
                    [:th "credentials"]
                    [:th "last modified"]]]
                [:tbody
                  (map
                    (fn [user]
                      [:tr {:key (:id user)}
                        [:td [:a {:href (routes/user {:id (:id user)})} (:username user)]]
                        [:td (:name user)]
                        [:td (:credential-source-id user)]
                        [:td (str (.toLocaleString (js/Date. (:last-modified user))))]])
                    (:results @results))]]))]

        [:div {:style {:text-align :center}}
          [pager
            {:page-size page-size :item-count (:total-count @results) :start-page @page
              :loading? (= :loading @results)
              :page-fn
                (fn [new-page]
                  (reset! page new-page)
                  (swap! search-params assoc :offset (* (dec new-page) page-size))) }]]
      ])))

(defn component
  []
  (let [vs (subscribe [:view-state])]
    (fn []
      (if (:id @vs)
        (if (= :new (:id @vs))
         [new-user]
         [edit-user (:id @vs)])
        [listing]))))
