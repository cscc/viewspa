(ns edu.unc.cscc.views.layout.edit-chart
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console :include-macros true]
            [ace]
            [interact]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.layout.acl :as acl]
            [edu.unc.cscc.views.layout.controls :as c]
            [edu.unc.cscc.views.layout.datasets :as ds]
            [edu.unc.cscc.views.remote-api :as remote]
            [edu.unc.cscc.views.routes :as routes]
            [edu.unc.cscc.views.layout.view-chart :as view-chart]))


;;;;
; TODO:
;   * make edit and new chart components share same structure
;   * pull out script editor into own file
;
;;;;



(defn edit-chart-script-component
  [chart]
  (let [vs (subscribe [:view-state])
        project-id (:project-id @vs)
        editor (r/atom nil)
        valid? (r/atom false)
        timeout-id (r/atom nil)
        script (subscribe [:resources/for-chart (:id chart) (:main-resource-path chart)])
        ; datasets
        dataset-data (view-chart/create-dataset-data-sub-map chart)
        ; obtained after mount
        min-height-px (r/atom nil)
        editor-intialized? (r/atom false)]
    (r/create-class {:display-name "edit-chart-script-component"
                      :reagent-render
                        (fn [chart]
                          (let [preview-source (r/atom "")
                                enable-preview? (r/atom false)
                                dirty-buffer? (r/atom false)
                                error (r/atom "")
                                submitted? (r/atom false)]
                            (fn [chart]
                              (defn- sync-buffer-on-change
                                []
                                (when @enable-preview?
                                  (when @timeout-id (.clearTimeout js/window @timeout-id))

                                  (reset! dirty-buffer? true)

                                  ; schedule update
                                  (reset! timeout-id
                                    (.setTimeout js/window
                                      (fn []
                                        (when @valid?
                                          (reset! preview-source (.getValue @editor))
                                          (reset! dirty-buffer? false))) 666))))
                              ; hack to initialize editor with script since
                              ; it might not exist in the sub'd atom when we actually
                              ; get around to rendering, and ace init happens after
                              ; our first render
                              (when (and @editor (not @editor-intialized?))
                                (when (not (= :loading @script))
                                  (reset! editor-intialized? true)
                                  (.setValue @editor @script)
                                  (.gotoLine @editor 0)
                                  (.clearSelection (.getSelection @editor))
                                  (reset! preview-source @script)
                                  (.on (.getSession @editor) "change"
                                    sync-buffer-on-change)))
                              [:div
                                {:class "row"}
                                [:section
                                  {:class "six columns"
                                    :style {:border-right "0.1rem #bbb dotted"
                                            :margin-bottom "2rem"}}
                                  [:section {:style {:padding-left "0.25rem"}}
                                    [:section
                                      [:h4 "source"]]

                                    [:section
                                      {:style {:padding-bottom "1rem"}}
                                      [:section
                                        {:id "editor-wrapper"
                                          :class "bottom-drag-handle"
                                          :style
                                            {:height "30rem"}}
                                        [:div {:id "chart-source-editor"
                                                :style {:height "100%"
                                                        :width "100%"}}]]]

                                    [:section {:class "clearfix"}
                                      [:button
                                        {:class "no-margin"
                                          :on-click #(routes/go! (routes/charts {:project-id project-id}))
                                          :style {}}
                                        "cancel"]
                                      [:button
                                        {:class "no-margin" :style {:float :right}
                                          :disabled (if @submitted? :disabled)
                                          :on-click
                                            (fn []
                                              (reset! error nil)
                                              (reset! submitted? true)
                                              (remote/resource-save
                                                {:chart-id (:id chart)
                                                  :path (:main-resource-path chart)
                                                  :content (.getValue @editor)
                                                  :on-success #(routes/go! (routes/charts {:project-id project-id}))
                                                  :on-failure
                                                    (fn []
                                                      (reset! submitted? false)
                                                      (reset! error "Failed to save chart source."))}))}
                                        (if @submitted? "saving..." "save")]]
                                    [:section {:class "error text-right"}
                                      @error]]]
                                [:section {:class "six columns"}
                                  [:section {:class "clearfix"}
                                    [:h4 {:style {:float :left}}
                                      (if @dirty-buffer?
                                        (if @valid? " (updating...)" " (suspended; invalid source)")
                                        "preview")]
                                    [:span {:style {:float :right}}
                                      [:h6
                                        {:style {:display :inline-block :margin-right "1rem" :margin-bottom 0}}
                                        "live update"]
                                      [:div
                                        [c/toggle-bar
                                          {:name "enable" :selected? @enable-preview?
                                            :on-select
                                              (fn []
                                                (reset! enable-preview? true)
                                                (sync-buffer-on-change))}
                                          {:name "disable" :selected? (not @enable-preview?) :on-select #(reset! enable-preview? false)}]]]]
                                  (when
                                    (and @editor-intialized?
                                      ; datasets must all be loaded
                                      (every? #(not (keyword? @(second %))) dataset-data))
                                    [view-chart/chart-component chart @preview-source
                                      ; deref all datasets
                                      (zipmap
                                        (keys dataset-data)
                                        (map #(deref %) (vals dataset-data))) ])]])))
                      :component-did-mount
                        (fn []
                          (let [e (.edit js/ace "chart-source-editor")]
                            (set! (.-x js/window) e)
                            (.setMode (.-session e) "ace/mode/javascript")
                            (.setAutoScrollEditorIntoView e true)
                            (set! (.-$blockScrolling e) js/Infinity)
                            (.resize e)
                            (reset! min-height-px (-> (.getElementById js/document "editor-wrapper") .-clientHeight))
                            (.
                              (.resizable (js/interact "#editor-wrapper")
                                (js-obj
                                  "preserveAspectRatio" false
                                  "edges" (js-obj "bottom" true)))
                              (on "resizemove"
                                (fn [event]
                                  (when (< @min-height-px (-> event .-rect .-height))
                                    (set! (-> event .-target .-style .-height) (str (-> event .-rect .-height) "px"))
                                    (.resize @editor)))))
                            ; listen for annotation changes, and check if any are
                            ; for errors
                            (.on (.getSession e) "changeAnnotation"
                              (fn []
                                (let [annot (.getAnnotations (.getSession e))]
                                  (reset! valid? (empty? (filter #(= "error" (.-type %)) annot))))))
                            (reset! editor e)))
                      :component-will-unmount
                        (fn []
                          (.destroy @editor)
                          (.remove (.-container @editor)))})))

(defn script-component
  [chart-id]
  (let [chart (subscribe [:charts chart-id])]
    (fn []
      [:section {:class "twelve columns"}
        (if (= :loading @chart)
          [:p "Loading..."]
          [edit-chart-script-component @chart])])))



;;;;;; TODO - role the edit and new components into one

(defn- dataset-picker
  [{:keys [start-expanded?]}]
  (let [expanded? (r/atom (true? start-expanded?))]
    (fn [{:keys [on-select on-remove datasets]}]
      [:div
        {:class "ten columns"}
        [:h4
          {:style
            {:margin 0
              :cursor :pointer}
            :on-click #(swap! expanded? not)}
          "Datasets"
          [:span {:style {:float :right :font-size "1rem" :margin-top "1rem"}}
            (if @expanded? "(hide)" "(show)"  )]]
        [:div
          {:style {:display (if @expanded? :block :none)}}
          [:p
            {:class "small"
              :style {:margin 0 :margin-bottom "0.25rem"}}
            "Please specify which datasets this chart will depend on.
            Datasets that are not selected here will not be available to the chart."]
          ; current datasets
          [:div
            [:h6 {:style {:margin 0}} "Depends on "
              (when (not (empty? datasets))
                [:span {:style {:font-size "60%"}} "(click to remove)"])]
            (if (empty? datasets)
              [:span "(none)"]
              (into [:div]
                (for [ds datasets]
                  [:span
                    {:style {:display :inline-block :margin-right "0.5rem"}}
                    [:a {:href "javascript:void(0)" :on-click #(on-remove ds)}
                      (:identifier ds)]])))]

          ; listing
          [:div {:style {:margin-top "1rem"}}
            [:h6 {:style {:margin 0}} "Available"]
            [ds/listing
              {:on-select on-select
                :filter
                  (fn [d] (not (some #(= (:id d) (:id %)) datasets)))}]]]
        [:hr
          {:style
            {:display (if @expanded? :none :block)
              :margin 0 :margin-top "0.5rem"
              :padding 0 :border-color "#ccc"}}]])))

(defn- acl-component
  [{:keys [start-expanded?]
    :or {start-expanded? false}}]
  (let [expanded? (r/atom (true? start-expanded?))
        user (subscribe [:current-user])]
    (fn [{:keys [acl on-change filter-current-user?]
          :or {filter-current-user? false}}]
      (let [current-user @user]
        [:div
          [:h4
            {:style
              {:margin 0
                :cursor :pointer}
              :on-click #(swap! expanded? not)}
            "Permissions"
            [:span {:style {:float :right :font-size "1rem" :margin-top "1rem"}}
              (if @expanded? "(hide)" "(show)"  )]]

          [:div {:style {:display (if @expanded? :block :none)}}
            [:p {:class "" :style {:margin 0}}
              "The permissions below will control who can modify the chart."]
            [:p {:style {:margin 0 :margin-bottom "0.5rem"}}
            "The ability to view a chart will be controlled by the ability to view "
            "the datasets associated with a chart."]
            [acl/component acl
              {:write "Modify Chart"}
              on-change
              (if filter-current-user? #(not (= (:id current-user) (:id %))) (fn [u] true))]]
          [:hr
            {:style
              {:display (if @expanded? :none :block)
                :margin 0 :margin-top "0.5rem"
                :padding 0 :border-color "#ccc"}}]]))))

(defn- new-chart
  [project-acl]
  (let [vs (subscribe [:view-state])
        user (subscribe [:current-user])
        name (r/atom "")
        description (r/atom "")
        datasets (r/atom [])
        uploading? (r/atom false)
        taken-names (r/atom [])
        js-file (r/atom nil)
        file-content (r/atom nil)
        file-error (r/atom nil)
        failure-message (r/atom "")
        acl (r/atom {:partial true})
        can-edit-acl? (auth/acl-allows? (:id @user) project-acl :manage-chart-access-control)]
    (fn []
      [:div
        [:div {:id "new-entity" :class "row"}
          [:div {:class "twelve columns"}
            [:h3 "New Chart"]

            [:fieldset {:class "row"}
              [:legend ""]
              [:label {:class "ten columns"}
               [:input {:type "text"
                        :placeholder "Chart name"
                        :value @name
                        :on-change
                          (fn [e]
                            (reset! name (-> e .-target .-value)))}]]
              [:label {:class "ten columns"}
               [:textarea {:placeholder
                           "Human-readable chart description.

Feel free to describe functionality, intended audience, etc."
                           :value @description
                           :on-change #(reset! description (-> % .-target .-value))}]]]


            [:fieldset {:class "row"}
              [:legend ""]
              [:div {:class "ten columns clearfix"}
                [:button
                  {:on-click #(routes/go! (routes/charts {:project-id (:project-id @vs)}))
                    :style {:float :left :margin-right "0.5rem" :margin-bottom "0.5rem"}}
                  "cancel"]

                [:div {:style {:float :right :max-width "100%"}}
                  [:label
                    {:class "file-input"
                      :style {:display :inline-block}}
                    [:span
                      {:class "contents"
                        :style {:margin 0}}
                      (if (some? @js-file)
                        (if (= :loading @file-content)
                          "Loading..."
                          [:span [:b "source: "] (.-name @js-file)])
                        (if @file-error
                          [:span {:style {:color :red}} @file-error]
                          "select chart"))]
                    [:input {:type "file"
                              :on-change (fn [evt]
                                          (reset! file-content nil)
                                          (reset! js-file nil)
                                          (reset! file-error nil)
                                          (let [file (aget (-> evt .-target .-files) 0)
                                                max-size (* 1024 1024 1)]

                                            (if (and file (< max-size (.-size file)))
                                              (reset! file-error (str "Chart must be < " (/ max-size 1024) " KB"))
                                              (when file
                                                (reset! js-file file)
                                                (reset! file-content :loading)
                                                (let [reader (js/FileReader.)]
                                                  (set! (.-onload reader)
                                                    #(reset! file-content (-> % .-target .-result)))
                                                  (.readAsText reader file))) )))
                              :disabled (= :loading @file-content)}]]

                  [:button
                      {:style {:float :right :margin-left "0.5rem" :margin-bottom "1rem" :margin-right 0}
                        :disabled (if
                                  (or
                                    (empty? @name)
                                    (empty? (str/trim @name))
                                    (nil? @js-file)
                                    @uploading?
                                    (some #(= @name %) @taken-names))
                                  :disabled)
                      :on-click (fn []
                                  (let []
                                    (reset! uploading? true)
                                    (remote/chart-save
                                      {:project-id (:project-id @vs)
                                        :name @name
                                        :description @description
                                        :main-resource-content @file-content
                                        :required-datasets (map #(:id %) @datasets)
                                        :on-duplicate-name
                                        (fn [name]
                                          (swap! taken-names conj name)
                                          (reset! uploading? false))
                                        :on-success
                                          (fn [c]
                                            (let [on-success
                                                  #(routes/go!
                                                    (routes/charts
                                                      {:project-id (:project-id @vs)}))]
                                              (if can-edit-acl?
                                                (remote/chart-acl-save
                                                  {:id (:id c)
                                                    :acl @acl
                                                    :on-success on-success
                                                    :on-failure
                                                      #(do
                                                        (reset! failure-message "failed to save ACL")
                                                        (reset! uploading? false))})
                                                (on-success))))
                                        :on-failure #(do
                                                        (reset! failure-message "failed to save chart")
                                                        (reset! uploading? false))})))}
                      (if @uploading? "saving..." "save")]]

                ]]
            [:div {:id "failure-message" }
              (if (some #(= @name %) @taken-names)
                [:p "Oops... that name is already in use."])
              [:p @failure-message]]
            (when can-edit-acl?
              [:div {:class "row" :style {:margin-bottom "1rem"}}
                [:div {:class "ten columns"}
                  [acl-component
                    {:acl @acl :on-change #(reset! acl %) :filter-current-user? true
                      :start-expanded? false}]]])
            ; datasets
            [:div {:class "row"}
              [dataset-picker
                {:on-select #(swap! datasets conj %)
                  :on-remove
                    (fn [d]
                      (swap! datasets #(remove #{d} %)))
                  :datasets @datasets :start-expanded? false}]]

            ]]])))

(defn- edit-component
  [c a project-acl]
  (let [chart (r/atom c)
        taken-names (r/atom [])
        acl (r/atom a)
        user (subscribe [:current-user])
        id (:id c)
        uploading? (r/atom false)
        failure-message (r/atom "")
        delete-allowed? (r/atom false)
        show-datasets? (r/atom false)
        can-edit-acl? (auth/acl-allows? (:id @user) project-acl :manage-chart-access-control)]
    (fn [_ _]
      [:div
        [:div {:id "new-entity" :class "row"}
          [:h3
           (str "Edit Chart: '" (:name @chart) "'")]
          [:div {:class "twelve columns"}
           [:fieldset {:class "row"}
            [:legend ""]
            [:label {:class "ten columns"}
             [:input {:type "text"
                      :placeholder "Chart name"
                      :value (:name @chart)
                      :on-change
                        (fn [e]
                          (swap! chart assoc :name (-> e .-target .-value)))}]]
            [:label {:class "ten columns"}
             [:textarea {:placeholder
                         "Human-readable chart description.

  Feel free to describe functionality, intended audience, etc."
                         :value (:description @chart)
                         :on-change #(swap! chart assoc :description (-> % .-target .-value))}]]]


            [:fieldset {:class "row"}
              [:legend ""]
              [:div {:class "ten columns clearfix"}
                [:button
                  {:disabled (if @uploading? "disabled")
                    :style (merge {:float :left :margin-bottom "1rem"} (if @delete-allowed? {:background-color :red :color :white}))
                    :on-click
                    (fn [e]
                      (.preventDefault e)
                      (if @delete-allowed?
                        (remote/chart-delete
                          {:id id
                            :on-success #(routes/go! (routes/charts {:project-id (:id (:project @chart))}))
                            :on-failure
                              #(do
                                (reset! uploading? false)
                                (reset! failure-message %))})
                        (swap! delete-allowed? not)))}
                  (if @delete-allowed?
                    "! DELETE !"
                    " delete ")]

                [:button
                    {:style {:float :right :margin-left "1rem" :margin-bottom "1rem"}
                      :disabled (if
                                (or
                                  (empty? (:name @chart))
                                  (empty? (str/trim (:name @chart)))
                                  @uploading?
                                  (some #(= (:name @chart) %) @taken-names))
                                :disabled)
                    :on-click (fn []
                                (reset! uploading? true)
                                (remote/chart-save
                                  {:project-id (:id (:project @chart))
                                    :id id
                                    :name (:name @chart)
                                    :description (:description @chart)
                                    :required-datasets (map #(:id %) (:datasets @chart))
                                    :on-duplicate-name
                                    (fn [name]
                                      (swap! taken-names conj name)
                                      (reset! uploading? false))
                                    :on-success
                                      (fn []
                                        (let [on-success
                                              #(routes/go!
                                                (routes/charts
                                                  {:project-id (:id (:project @chart))}))]
                                          (if can-edit-acl?
                                            (remote/chart-acl-save
                                              {:id id
                                                :acl @acl
                                                :on-failure
                                                  #(do
                                                    (reset! failure-message "failed to save ACL")
                                                    (reset! uploading? false))
                                                :on-success on-success})
                                            (on-success))))
                                    :on-failure #(do
                                                    (reset! failure-message "failed to save chart")
                                                    (reset! uploading? false))}))}
                    (if @uploading? "saving..." "save")]
                (when true
                  [:button
                    {:on-click
                      (fn []
                        (routes/go!
                          (routes/chart
                            {:id (:id @chart)
                              :project-id (:id (:project @chart))
                              :query-params {:action "edit-script"}})))
                      :style {:float :right :margin-left "1rem"}}
                    "edit source"])
                  ]]
            [:div {:id "failure-message" }
              (when (some #(= (:name @chart) %) @taken-names)
                [:p "Oops... that name is already in use."])
              (if (some? @failure-message)
                [:p @failure-message])]
            (when can-edit-acl?
              [:div {:class "row" :style {:margin-bottom "1rem"}}
                [:div {:class "ten columns"}
                  [acl-component
                    {:acl @acl :on-change #(reset! acl %) :filter-current-user? false
                      :start-expanded? false}]]])
            [:div {:class "row"}
              [dataset-picker
                {:on-select
                    (fn [d]
                      (swap! chart update :datasets conj d))
                  :on-remove
                    (fn [d]
                      (swap! chart update :datasets #(remove #{d} %)))
                  :datasets (:datasets @chart) :start-expanded? false}]]
            ]]])))


(defn- edit-chart
  [id]
  (let [chart (subscribe [:charts id])
        acl (subscribe [:charts/acl id])
        user (subscribe [:current-user])
        project-id (subscribe [:view-state :project-id])
        project-acl (subscribe [:projects/acl-for-user (:id @user)] [project-id])]
    (fn []
      [:div
        (if (or (= :loading @chart) (= :loading @acl) (= :loading @project-acl))
          [:div "Loading..."]
          [edit-component @chart @acl @project-acl])])))

(defn component
  []
  (let [chart-id (subscribe [:view-state :id])
        user (subscribe [:current-user])
        project-id (subscribe [:view-state :project-id])
        project-acl (subscribe [:projects/acl-for-user (:id @user)] [project-id])]
    (fn []
      [:div
        (if (= :new @chart-id)
          (if (= :loading @project-acl)
            [:div "Loading..."]
            [new-chart @project-acl])
          [edit-chart @chart-id])])))
