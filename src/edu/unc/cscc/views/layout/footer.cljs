(ns edu.unc.cscc.views.layout.footer
  (:require
    [edu.unc.cscc.views.routes :as routes]))

(defn component
  []
  (fn []
    [:footer
     [:div {:id "logo-block"}
      [:a {:href "http://cscc.io"}
       [:img {:src "images/bar-logo-scaled.png"}]]
      [:br]
      [:a {:id "unc-mark"
           :href "http://www.unc.edu/"}
       [:img {:src "images/unc-mark-black.png"}]]]
     [:div {:class "row legalese"}
      [:div {:class "three columns offset-by-one"}
       [:p "© 2016 - "
        "CSCC - "
        [:a {:href "http://sph.unc.edu/bios/biostatistics/"} "BIOS"]
        " - "
        [:a {:href "http://sph.unc.edu/"} "SPH"]
        " - "
        [:a {:href "http://www.unc.edu"} "UNC"]]]
      [:div {:class "four columns"}]
      [:div {:class "three columns"}
       [:p [:a {:href (routes/about)} "views"]" is an open source project from CSCC."]
       [:p [:a {:href "/LICENSE" :target "_blank"} "Licensed under the BSD license."]]]]]))
