(ns edu.unc.cscc.views.layout.view-chart
  (:require
     [reagent.core :as r]
     [reagent.impl.util :as ru]
     [re-frame.core :refer [dispatch
                            dispatch-sync
                            subscribe]]
     [shodan.console :as console]

     ; JS
     [webCharts] ; note that this IS NOT the actual name of the webCharts object
     [d3]
     [d3-promise]

     [edu.unc.cscc.views.config :as config]
     [edu.unc.cscc.views.remote-api :as remote]))


(defn create-dataset-data-sub-map
 "build a map of dataset identifiers to atoms w/ subscriptions to
 dataset data for the datasets referenced by the given chart"
 [chart]
 (zipmap
   (map #(:identifier %) (:datasets chart))
   (map #(subscribe [:datasets/data (:id %)]) (:datasets chart))))


; ==============================================================================
; NOTE: we use numerous hacks here since we're going between the nice clean
; reagent world and webcharts/d3 which expect some level of DOM control.
;
; The basic idea is that the reagent component sets up the UI stuff around where
; the chart renders and puts up a "loading..." type of message, then the
; chart gets refreshed on prop changes or mount, and renders into the same area
; that the loading message is in (blowing it away).
;
; We also have one particularly kludgy bit: we move window.webCharts to
; a different variable (defined in config) to prevent clever programmers from
; trying to use the webCharts API directly and scribbling all over our React
; components.
; ==============================================================================

(defn- create-main-promise
 "takes the main entry fn of a chart and the options to pass to it
 and produces a promise which we can chain all future execution from"
 [main-fn options]
 (js/Promise.
   (fn [resolve reject]
     (if (.-call main-fn)
       (resolve (main-fn options))
       (reject "main was not function")))))

;;;;; BRIDGE functions

; these are functions that create functions which will be passed to our
; chart's main entry point

(defn- bridge-legacy-create-chart
 [js-wc-proxy]
 (fn [config control-config]
   (let [controls (.createControls js-wc-proxy nil control-config)]
     (.createChart js-wc-proxy nil config controls))))

(defn- bridge-wc-proxy
 [{:keys [id js-wc on-chart on-controls on-table]
   :or {on-chart #() on-controls #() on-table #()}}]
 (js-obj
   "createChart"
   (fn [element config controls]
     (let [chart (.createChart js-wc (str "#canvas-" id) config controls)]
       (on-chart chart)
       chart))
   "createControls"
   (fn [element config]
     (let [controls (.createControls js-wc (str "#controls-" id) config)]
       (on-controls controls)
       controls))
   "createTable"
   (fn [element config controls]
     (let [table (.createTable js-wc (str "#table-" id) config controls)]
       (on-table table)
       table))
   "multiply" (.-multiply js-wc)
   "dataOps" (.-dataOps js-wc)))

(defn- bridge-fetch-dataset
 "create a bridge function (which returns a js/Promise) for loading a given
 dataset via identifier.  The 2-arity form will load the dataset while the
 1-arity form will only 'load' from a given map of datasets."
 ([chart on-load]
   (fn [dataset-identifier]
     (js/Promise.
       (fn [resolve reject]
         (if (not (some #(= dataset-identifier (:identifier %)) (:datasets chart)))
           (reject
             (str "The dataset with the identifier '" dataset-identifier
               "' is not accessible to this chart."))
           (remote/dataset-load-data
             {:identifier dataset-identifier
               :project-id (:id (:project chart))
               :on-success
               (fn [dataset]
                 (on-load dataset)
                 (resolve dataset))
               :on-failure reject}))))))
 ([datasets]
   (fn [dataset-identifier]
     (js/Promise.
       (fn [resolve reject]
         (if (contains? datasets dataset-identifier)
           (resolve (get datasets dataset-identifier))
           (reject
             (str "No dataset found with identifier '" dataset-identifier "'"))))))))


(defn chart-component
 [chart main-resource initial-dataset-data]
 (let [chart-loading? (r/atom true)
       chart-error (r/atom nil)
       current-resource (r/atom main-resource)
       current-chart (r/atom chart)
       dataset-data (r/atom (if (map? initial-dataset-data) initial-dataset-data {}))
       js-wc (aget js/window config/web-charts-var-name)
       destruct-queue (r/atom [])
       queue-for-destruction #(swap! destruct-queue conj %)
       destroy-all 
         (fn []
           (while (seq @destruct-queue)
             (let [c (last @destruct-queue)]
               (swap! destruct-queue pop)
               (when (aget c "destroy")
                 (.destroy c)))))
       refresh-chart
       (fn [new-chart new-resource]
         (destroy-all)
         ; HACK - nuke chart and controls
         (set!
           (.-innerHTML
             (.getElementById js/document (str "canvas-" (:id @current-chart))))
           "")
         (set!
           (.-innerHTML
             (.getElementById js/document (str "controls-" (:id @current-chart))))
           "")
         (set! 
           (.-innerHTML
             (.getElementById js/document (str "table-" (:id @current-chart))))
           "")
         (reset! chart-error nil)
         (reset! chart-loading? true)
         (reset! current-chart new-chart)
         (reset! current-resource new-resource)
         (let [chart-id (:id new-chart)
               js-wc-proxy
                 (bridge-wc-proxy 
                   {:id chart-id :js-wc js-wc
                     :on-chart queue-for-destruction
                     :on-table queue-for-destruction
                     :on-controls queue-for-destruction})]
           (try
             (.
               (create-main-promise
                 (.eval js/window (str "(function(){ return " new-resource "})()"))
                 (js-obj
                   "interactive" true
                   "webCharts" js-wc-proxy
                   "createChart" (bridge-legacy-create-chart js-wc-proxy)
                   "fetchDataset"
                     (if initial-dataset-data
                       (bridge-fetch-dataset @dataset-data)
                       (bridge-fetch-dataset
                         chart
                         (fn [ds] (swap! dataset-data assoc (:identifier ds) ds))))))
               (then
                 (fn []
                   (reset! chart-loading? false))
                 (fn [reason]
                   (let [reason
                         (if (not reason)
                           "Chart execution failed. That's all we know."
                           reason)]
                     (reset! chart-loading? false)
                     (reset! chart-error (str reason))
                     (console/error "exec failed")))))
             (catch :default e
               (reset! chart-error (str e))))))]

   (r/create-class {:display-name "chart-component"
                   :reagent-render
                     (fn [chart main-resource _]
                       (fn [chart main-resource _]
                         [:div
                           [:div
                             {:id "chart-progress"
                               :style {:display (if (and @chart-loading? (not @chart-error)) :block :none)}}
                             "Rendering chart..."]
                           [:div
                             {:id "chart-error"
                               :style {:display (if @chart-error :block :none)}}
                             [:img {:id "dead-chart"
                                     :src "/images/dead-chart.png"
                                     :style
                                       {:display :block
                                         :max-width 400
                                         :min-width 100
                                         :width "100%"
                                         :margin-bottom "1rem"}}]
                             [:h5 "Something went wrong!"]
                             [:p @chart-error]]
                           [:div
                             {:id "chart-render-target"
                               :style
                                 (if (or @chart-loading?)
                                   {:visibility :hidden :z-index "-10000"}
                                   (if @chart-error
                                     {:display :none})) }
                             [:div
                               {:id (str "controls-" (:id chart))
                                 :style {:margin-bottom "1rem"}}]
                             [:div {:id (str "canvas-" (:id chart))}]
                             [:div {:id (str "table-" (:id chart))}]]]))
                   :component-did-mount
                     (fn [this]
                       (refresh-chart chart main-resource))
                   :component-will-receive-props
                     (fn [this new-argv]
                       (let [args (rest new-argv)
                             new-chart (first args)
                             new-resource (second args)]
                         (when (or (not (= new-chart @current-chart))
                                 (not (= new-resource @current-resource)))
                           (refresh-chart new-chart new-resource))))
                   :component-will-unmount destroy-all})))

(defn- chart-container
 [chart]
 (let [main-resource (subscribe [:resources/for-chart (:id chart) (:main-resource-path chart)])]
   (fn [chart]
     [:div
       (if (= :loading @main-resource)
         [:p "Loading main resource..."]
         [chart-component chart @main-resource])])))

(defn component
 [id]
 (let [chart (subscribe [:charts id])]
   (fn [id]
     [:div
       (if (= :loading @chart)
         [:div "Loading chart..."]
         [:div {:id "chart-content"}
           [:h5 (:name @chart)]
           [:p {:class "description"} (:description @chart)]
           [chart-container @chart]])])))
