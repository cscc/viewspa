(ns edu.unc.cscc.views.layout.acl
  (:require
    [clojure.string :refer [lower-case]]
    [reagent.core :as r]
    [shodan.console :as console]
    [re-frame.core :refer [subscribe]]

    [edu.unc.cscc.views.layout.controls :as c]
    [edu.unc.cscc.views.auth :as auth]))


(def ^:private permission-table
  {:perm-keyword "Sample permission"
    :some-other-thing "Other permission"
    :yet-another-thing "Yet another"})

(def ^:private sample-acl
  {:all-users {:some-other-thing true}
    :anonymous {}
    :user-entries
      {-1
        {:user {:id -1 :username "test" :name "Test User"}
          :entries {:perm-keyword true}}
        -2
          {:user {:id -2 :username "test2" :name "Another Test User"}
            :entries {:perm-keyword true}}
        -3
          {:user {:id -3 :username "test3" :name "Third Test User"}
            :entries {:perm-keyword true}}} })

(defn- private-user-row
  [user all-permissions entries on-grant-deny]
  (assert (or (:username user) (:name user)))
  [:div {:class "clearfix"}
    [:span
      {:style {:float :left :margin-right "1rem" :max-width "15rem"}}
      (if (and (:name user) (not (empty? (:name user))))
        (str (:name user)
          (when (:username user)
            (str " (" (:credential-source-id user) "/" (:username user) ")")))
        (:username user))]
    [:div {:style {:float :right}}
      (into [:div {:style {:text-align :right}}]
        (map
          (fn [p]
            [:div {:style {:display :inline-block :padding "0.2rem"}}
              [c/combo-bar {:name (last p) :selected? (true? (get entries (first p)))
                            :on-click #(on-grant-deny (first p) %)}]])
        all-permissions))]])

(defn-
  user-contains?
  "Test if a given user contains a given string. Returns a transducer when no
  user is supplied."
  ([str]
    (let [pattern (re-pattern (lower-case str))]
      (fn [user]
        (or (re-find pattern (lower-case (:name user)))
          (re-find pattern (lower-case (:username user)))))))
  ([str user]
    ((user-contains? str) user)))

(defn component
  [_ _ _ _]
  (let [search (r/atom "")
        search-params (r/atom {:term "" :limit 10})
        ; HACK! BUG! This is only safe so long as current-user isn't expected
        ; to lazy load
        user (subscribe [:current-user])
        search-results (subscribe [:users/search] [search-params])
        interval-id (r/atom nil)]
    (fn [full-acl permissions on-change user-filter]
      (let [acl
              (if (empty? @search)
                full-acl 
                (auth/acl-filter-users full-acl (user-contains? @search)))
            acl-users (auth/acl-get-users acl)]
        [:div
          {:class "acl"}
          [:div
            {:class "search"
              :style {:margin-bottom "1rem"}}
            [:input
              {:type :search
                :placeholder "user search"
                :style {:width "100%" :margin 0}
                :value @search
                :on-change
                  (fn [e]
                    (reset! search (-> e .-target .-value))
                    ; if search was exact match for username, scroll into view
                    (let [filtered
                            (filter
                              (fn [e]
                                (= @search (:username (:user (second e)))))
                            (:user-entries acl))]
                      (when (not (empty? filtered))
                        (.scrollIntoView (.getElementById js/document (str "user-" (first (first filtered)))))))
                    ; delay swap into search params to avoid spamming sub
                    (when (not @interval-id)
                      (reset! interval-id
                        (.setTimeout js/window
                          (fn []
                            (swap! search-params assoc :term @search)
                            (reset! interval-id nil)) 666))))}]
            ; results
            (let [users (atom (:results @search-results))
                  filtered-results
                    (filter
                      (fn [user]
                        (and
                          ; only show users not already in the ACL
                          (not (auth/acl-represents? acl user))
                          (if user-filter (user-filter user) true)))
                      (if (or (vector? @users) (seq? @users)) @users []))
                  show-results?
                    (and (not (empty? @search))
                      (or (= :loading @search-results) (not (empty? filtered-results))))]
              (when show-results?
                [:div {:class "search-results"}
                  (if (or (= :loading @search-results) (not (= @search (:term @search-params))))
                    [:p "Searching..."]
                    (into [:div]
                      (for [user filtered-results]
                        [:a {:class "result" :style {:color :black}
                              :href "javascript:void(0)"
                              :on-click
                              (fn []
                                (on-change (auth/acl-add user full-acl {}))
                                ; hack - set a timeout to scroll to focus on new user row
                                (.setTimeout js/window
                                  (fn []
                                    (.scrollIntoView (.getElementById js/document (str "user-" (:id user))))) 333))}
                          (if (and (:name user) (not (empty? (:name user))))
                            (str (:name user) " (" (:credential-source-id user) "/" (:username user) ")")
                            (:username user))])) )]))
          ]
          (when (< 100 (count acl-users))
            [:div
              {:style {:margin-bottom "0.5rem"}}
              "(Note: only displaying first 100 users.  Use search to refine your query.)"])
          ; users in acl
          (into
            [:div {:class "entries"}
              [:div {:class "user"}
                [private-user-row {:name "Everyone"} permissions (auth/acl-get-permissions acl :anonymous)
                (fn [permission granted?]
                  (on-change
                    (if granted?
                      (auth/acl-grant :anonymous full-acl permission)
                      (auth/acl-deny :anonymous full-acl permission))))]]
              [:div {:class "user"}
                [private-user-row {:name "All Users w/ Accounts"} permissions (auth/acl-get-permissions acl :all-users)
                  (fn [permission granted?]
                    (on-change
                      (if granted?
                        (auth/acl-grant :all-users full-acl permission)
                        (auth/acl-deny :all-users full-acl permission))))]]]
            (for [user (take 100 acl-users)]
              (do
                (let [entries (auth/acl-get-permissions full-acl (:id user))]
                  [:div {:class "user" :id (str "user-" (:id user))}
                    [private-user-row user permissions entries
                      (fn [permission granted?]
                        (on-change
                          (if granted?
                            (auth/acl-grant (:id user) full-acl permission)
                            (auth/acl-deny (:id user) full-acl permission))) )]])) )
          )
          (when (not (empty? @search))
            [:div
              {:style {:margin-bottom "0.5rem"}}
              "(Displayed ACL restricted to users matching search)"])]))))
