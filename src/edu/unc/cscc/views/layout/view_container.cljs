(ns edu.unc.cscc.views.layout.view-container
  (:require
    [re-frame.core :refer [subscribe]]

    [edu.unc.cscc.views.layout.about :as about]
    [edu.unc.cscc.views.layout.nav :as nav]
    [edu.unc.cscc.views.layout.login :as login]
    [edu.unc.cscc.views.layout.charts :as charts]
    [edu.unc.cscc.views.layout.dashboards :as dashboards]
    [edu.unc.cscc.views.layout.projects :as projects]
    [edu.unc.cscc.views.layout.datasets :as datasets]
    [edu.unc.cscc.views.layout.users :as users]))


(defn component
  [view-name]
  (let [view-state (subscribe [:view-state])]
    (fn [view-name]
      ; FIXME - show-nav should have sane default, which would eliminate need for
      ; this hack and other hackery
      (let [show-nav? (get-in @view-state [:show-nav] (not (= :login view-name)))]
        [:div
          [:section {:id "content" :class "row"}
           (when show-nav?
            [nav/component ["three columns no-mobile"]])
           (condp = view-name
                :limbo (fn [] [:div])
                :login [login/component]
                :charts [charts/component]
                :datasets [datasets/component]
                :dashboards [dashboards/component]
                :projects [projects/component]
                :users [users/component]
                :about [about/component])]
           (when show-nav?
              [nav/component ["yes-mobile"]])]))))
