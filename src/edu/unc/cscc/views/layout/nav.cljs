(ns edu.unc.cscc.views.layout.nav
  (:require [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console :include-macros true]
            [reagent.core :as r]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.routes :as routes]))

(defn component
  [extra-classes]
  (let [view (subscribe [:active-view])
        project-id (subscribe [:view-state :project-id])
        project (subscribe [:projects] [project-id])
        user (subscribe [:current-user])]
    (fn
      [extra-classes]
      [:nav {:class extra-classes}
      (when @user
        [:ul
          (when @project
            [:li {:style {:margin 0}}
              [:h5 {:style {:margin 0}} (:name @project)]
              [:ul
                [:li {:class (if (= :dashboards @view) "active")}
                  [:a {:href (routes/dashboards {:project-id (:id @project)})}
                  "dashboards"]]
                [:li {:class (if (= :charts @view) "active")}
                  [:a {:href (routes/charts {:project-id (:id @project)})}
                  "charts"]]
                [:li {:class (if (= :datasets @view) "active")}
                  [:a {:href (routes/datasets {:project-id (:id @project)})}
                  "data"]]]])
          [:li {:class (if (= :projects @view) "active")}
            [:a {:href (routes/projects)}
             "projects"]]
          (if (auth/can-manage-users? @user)
            [:li {:class (if (= :users @view) "active")}
              [:a {:href (routes/users)}
              "users"]])
          [:li {:class (if (= :about @view) "active")}
            [:a {:href (routes/about)} "about"]]] ) ])))
