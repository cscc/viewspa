(ns edu.unc.cscc.views.layout.projects
  (:require
    [reagent.core :as r]
    [re-frame.core :refer [dispatch subscribe]]
    [clojure.string :as string]
    [shodan.console :as console]
    [cljs-hash.md5 :refer [md5]]

    [edu.unc.cscc.views.layout.acl :as acl]
    [edu.unc.cscc.views.auth :as auth]
    [edu.unc.cscc.views.routes :as routes]
    [edu.unc.cscc.views.remote-api :as remote]
    [edu.unc.cscc.views.layout.pager :refer [pager]]))

(def ^:private private-project-permissions
  {:read "Read", :write "Modify"
    :create-charts "Create Charts"
    :create-datasets "Create Datasets"
    :manage-chart-access-control "Manage Chart Permissions"
    :manage-dataset-access-control "Manage Dataset Permissions"})

(defn- edit-component
  [p acl]
  (let [project (r/atom p)
        acl (r/atom acl)
        user (subscribe [:current-user])
        taken-names (r/atom ["testtesttest"])
        can-delete? (r/atom false)
        submitted? (r/atom false)
        error (r/atom nil)
        can-edit-project? (or (not (:id @project)) (auth/acl-allows? (:id @user) @acl :write))
        can-edit-acl? (or (not (:id @project)) (auth/can-manage-project-permissions? @user))]
    (fn []
      [:div {:id "new-entity" :class "row eight columns"}
        (when can-edit-project?
          [:h3 (if (:id @project)
              (str "Editing: " (:name @project))
              (if (empty? (:name @project))
                "New project"
                [:span "Creating Project: " (:name @project)]))])
        [:fieldset
          (if (not can-edit-project?)
            [:h5 "Project name: " (:name @project)]
            [:input
              {:type "text" :placeholder "project name"
                :value (:name @project)
                :on-change #(swap! project assoc :name (-> % .-target .-value))}])
          (if (not can-edit-project?)
            [:p "Project description: "
              (if (:description @project) (:description @project) "none")]
            [:textarea
              {:placeholder "A description of your shiny new project.

              You can write as much or as little as you'd like."
                :value (:description @project)
                :on-change #(swap! project assoc :description (-> % .-target .-value))}])]

        [:div {:class "clearfix" :style {:margin-top "1rem"}}
          [:div {:class "text-right" :style {:float :right :margin-bottom "0.75rem"}}
            [:span {:class "error" :style {:margin 0 :margin-right "1rem"}}
              @error
              (when (some #(= (:name @project) %) @taken-names)
                "name already in use")]
            [:button {:on-click #(routes/go! (routes/projects))} "cancel"]
            [:button
              {:style {:margin-left "0.5rem"}
                :disabled
                  (or @error
                      @submitted?
                      (empty? (:name @project))
                      (some #(= (:name @project) %) @taken-names))
                :on-click
                (fn []
                  (reset! submitted? true)
                  (let [acl-save
                        (fn [id]
                          (remote/project-acl-save
                            {:id id
                              :acl (if (:id @project) @acl (assoc @acl :partial true))
                              :on-success #(routes/go! (routes/projects))
                              :on-failure
                                (fn []
                                  (reset! error "failed to save project ACL")
                                  (reset! submitted? false))}))]
                    (if can-edit-project?
                      (remote/project-save
                        {:project @project
                          :on-success
                            (fn [id]
                              ; now save the acl
                              (if can-edit-acl?
                                (acl-save id)
                                (routes/go! (routes/projects))))
                          :on-failure
                            (fn []
                              (reset! error "failed to save project")
                              (reset! submitted? false))
                          :on-duplicate-name
                          (fn [name]
                            (swap! taken-names conj name)
                            (reset! submitted? false))})
                      (if can-edit-acl?
                        (acl-save (:id @project))))
                    ))}
              (if @submitted? "saving..." "save")]]
          [:div {:style {:float :left}}
            (when (:id @project)
              [:button
                {:disabled (or @submitted? (not can-edit-project?))
                  :style (merge {:float :left :margin-bottom "1rem"} (if @can-delete? {:background-color :red :color :white}))
                  :on-click
                  (fn [e]
                    (.preventDefault e)
                    (if @can-delete?
                      (remote/project-delete
                        {:id (:id @project)
                          :on-success #(routes/go! (routes/projects))
                          :on-failure
                            #(do
                              (reset! submitted? false)
                              (reset! error "failed to delete project"))})
                      (swap! can-delete? not)))}
                (if @can-delete?
                  "! DELETE !"
                  " delete ")])

            ]]
      (when (not can-edit-project?)
        [:p {:style {:padding 0 :margin "0.5rem 0" :color "#a22"}}
          "You cannot modify this projects properties, but you " [:u "can"] " modify its permissions."])
      (when can-edit-acl?
        [:div
          [:h4 {:style {:margin 0}} "permissions"]
          (if (or (= :loading @acl) (not (map? @user)))
            [:p "Loading..."]
            [acl/component @acl
              private-project-permissions
              #(reset! acl %)
              #(not (= (:id @user) (:id %)))])])])))

(defn- edit
  [id user]
  (let [project (if (= :new id) (r/atom {}) (subscribe [:projects id]))
        acl (if (= :new id)
              (r/atom {})
              (subscribe [:projects/acl id]))]
    (fn [id]
      [:div
        (if (or (= :loading @project) (= :loading @acl))
          [:p "Loading..."]
          (if (or (= :error @project) (= :error @acl))
            [:p {:class "error"} "Error loading project"]
            [edit-component @project @acl]))])))

(defn- djb2
  [s]
  (reduce
    (fn [h c] (+ (+ (bit-shift-left h 5) h) (int c)))
    5381
    (seq s)))

(defn- color->css
  [{:keys [red green blue]}]
  (str "rgba(" red ", " green "," blue ", 0.8)"))

(defn- generate-color
  [project]
  (let [hash (djb2 (md5 (:name project)))
        r (bit-shift-right (bit-and hash 0xFF0000) 16)
        g (bit-shift-right (bit-and hash 0x00FF00) 8)
        b (bit-and hash 0x0000FF)]
    {:red r :green g :blue b}))

(defn- yiq
  [{:keys [red green blue]}]
  (/ (+ (* red 299) (* green 587) (* blue 114)) 1000))

(defn- generate-contrast
  [color]
  (if (< 128 (yiq color)) {:red 0 :green 0 :blue 0} {:red 255 :green 255 :blue 255}))

(defn- project-entry
  [_ _]
  (fn [project can-modify?]
    (let [project-color (generate-color project)]
      [:div
        {:class "four columns noselect"
          :style {:cursor :pointer}
          :on-click #(routes/go! (routes/dashboards {:project-id (:id project)}))}
        ; icon
        [:div
          {:style
            {:width "100%" :height "10rem"
              :color (color->css (generate-contrast project-color))
              :background-color (color->css project-color)
              :outline "1px #ccc solid"

              :text-align :center
              :display :table
              :position :relative}}
          [:div
            {:style {:display :table-cell :vertical-align :middle}}
            [:span
              {:class "no-only-mobile" :style
                {:font-size "8rem"
                  :line-height "8rem"}}
              (string/upper-case (get (:name project) 0))]]
          [:div
            {:style
              {:display :table-cell
                :vertical-align :middle}}
            [:span
              {:class "yes-mobile"
                :style
                {:font-size "4rem"
                  :line-height "4rem"
                  :padding "1rem"}}
              (:name project)]]
          (when can-modify?
            [:button
              {:class (if (< 128 (yiq project-color)) "black-bg" "white-bg") :style
                {:position :absolute
                  :top 0
                  :right 0
                  :margin 0
                  :padding "0 0.25rem"
                  :border 0
                  :font-size "1.6rem"
                  :font-weight :bold
                  :color :inherit}
                :on-click
                  (fn [e]
                    (.preventDefault e)
                    (.stopPropagation e)
                    (routes/go! (routes/project {:id (:id project)}))
                    false)}
              "☰"])]
        [:h5 {:class "no-mobile" :style {:margin 0}} (:name project)]
        [:div
          {:style
            {:margin "0.25rem 0"
              :padding "0 0.25rem"
              :font-size "10pt"
              :line-height 1.2}}
          (:description project)] ])))

(defn listing
  []
  (let [term (r/atom "")
        user (subscribe [:current-user])
        page (r/atom 1)
        page-size 9
        params
          (r/atom {:term "" :with-user-acl? true 
                    :limit page-size :offset 0})
        projects (subscribe [:projects/search] [params])
        exec-search
          (fn [term]
            (reset! page 1)
            (swap! params assoc :term term :offset 0))]
    (fn []
      [:div {:class "eight columns"}
        ; search bar
        [:div {:class "text-right" :style {:margin-bottom "1rem"}}
          (when (auth/can-create-projects? @user)
            [:section {:style {:float :left}}
              [:button
                {:class "no-mobile-hd"
                  :style {:margin-left 0}
                  :on-click #(routes/go! (routes/project {:id "new"}))}
                "new project"]
              [:button
                {:class "yes-mobile-hd"
                  :style {:margin-left 0}
                  :on-click #(routes/go! (routes/project {:id "new"}))}
                "new"]])
          [:input
            {:type :search :placeholder "search"
              :on-change #(reset! term (-> % .-target .-value))
              :on-key-down
              (fn [e]
                (when (= 13 (.-keyCode e))
                  (exec-search @term)))}]
          [:button
            {:class "no-mobile-hd"
              :style {:margin-right 0}
              :on-click #(exec-search @term)
              :disabled (if (= @term (:term @params)) :disabled)}
            "search"]
          [:button
            {:class "yes-mobile-hd"
              :style {:margin-right 0}
              :on-click #(swap! params assoc :term @term)
              :disabled (if (= @term (:term @params)) :disabled)}
            "🔎"]]
        [:div {:class "row"}
          (if (= :error @projects)
            [:p {:class "error"} "Error loading projects"]
            (if (= :loading @projects)
              [:p "Loading..."]
              (if (or (not (map? @projects))
                    (empty? (:results @projects)))
                [:p "No projects found. "
                  (when (auth/can-create-projects? @user)
                    [:span "Would you like to "
                      [:a {:href (routes/project {:id "new"})} "create one?"]])]
                (into [:div {:class "twelve columns"}]
                  (for [row-contents (partition 3 3 nil (:results @projects))]
                    ^{:key row-contents}
                    (into
                      [:div {:class "row"}]
                      (for [project row-contents]
                        ^{:key project}
                        (let [acl (get (:acls @projects) (:id project))
                              show-edit-icon
                                (or (auth/acl-allows? (:id @user) acl :write)
                                  (auth/can-manage-project-permissions? @user))]
                          [project-entry project show-edit-icon]))))))))
        [:hr {:style {:width "100%" :margin-bottom 0}}]
        
        [:div {:style {:margin "0 auto" :padding 0 :text-align :center}} 
          [pager
            {:page-size page-size :item-count (:totalCount @projects) :start-page @page 
              :loading? (= :loading @projects)
              :page-fn
                (fn [new-page]
                  (reset! page new-page)
                  (swap! params assoc :offset (* (dec new-page) page-size))) }]] ]])))

(defn component
  []
  (let [vs (subscribe [:view-state])
        user (subscribe [:current-user])]
    [:div
      (if (= :loading @user)
        [:p "Loading..."]
        (if (:id @vs)
          [edit (:id @vs) @user]
          [listing]))]))
