(ns edu.unc.cscc.views.layout.about
  (:require
    [webCharts]
    [d3]
    [ace]

    [edu.unc.cscc.views.config :as config]))

(defn component
  []
  [:section {:class "row"}
    [:section {:class "two columns"}]
    [:section {:class "eight columns" :style {:padding-left "1rem"}}
      [:h3 {:style {:margin-bottom 0}} "about " [:b "views"]]
      [:h6 "version " config/product-version]

      [:p [:b "views"] " is an open-source data visualization management "
        "application developed by CSCC. It makes use of a number of open source
        projects, the versions/licensing of which are displayed below."]

      [:table
        [:thead
          [:tr
            [:th "library name"]
            [:th "version"]
            [:th "license"]]]
        [:tbody
          [:tr
            [:td [:a {:href "https://github.com/RhoInc/Webcharts"} "WebCharts"]]
            [:td (.-version (aget js/window config/web-charts-var-name))]
            [:td
              [:a {:href "https://raw.githubusercontent.com/RhoInc/Webcharts/master/LICENSE.md"}
                "MIT"]]]
          [:tr
            [:td [:a {:href "https://d3js.org"} "D3"]]
            [:td (.-version js/d3)]
            [:td [:a {:href "https://raw.githubusercontent.com/d3/d3/master/LICENSE"} "BSD"]]]
          [:tr
            [:td [:a {:href "https://github.com/ajaxorg/ace"} "Ace editor"]]
            [:td (.-version js/ace)]
            [:td [:a {:href "https://raw.githubusercontent.com/ajaxorg/ace/master/LICENSE"} "BSD"]]]
          [:tr
            [:td [:a {:href "https://github.com/taye/interact.js"} "interact.js"]]
            [:td "1.3.4"]
            [:td [:a {:href "https://raw.githubusercontent.com/taye/interact.js/master/LICENSE"} "MIT"]]]]]
      [:p [:b "views"] " is licensed under the 3-clause BSD license:"]
      [:blockquote {:style {:font-size "9pt" :line-height "9pt"}}
        "Copyright (c) 2016 - ," (.getFullYear (js/Date.)) [:br]
        "Collaborative Studies Coordinating Center" [:br]
        "Dept. of Biostatistics, UNC Chapel Hill" [:br]
        "Contributors: " [:br]
        [:span "Rob Tomsick (rtomsick" [:span {:style {:display :none}} "REMOVETHIS"]"@unc.edu)"] [:br]
        [:span "Rob Tomsick (robert" [:span {:style {:display :none}} "REMOVETHIS"]"@tomsick.net)"] [:br]
        [:br]
        "All rights reserved." [:br]
        [:br]
        "Redistribution and use in source and binary forms, with or without" [:br]
        "modification, are permitted provided that the following conditions are" [:br]
        "met:" [:br]
        [:br]
        "1. Redistributions of source code must retain the above copyright" [:br]
        "notice, this list of conditions and the following disclaimer." [:br]
        [:br]
        "2. Redistributions in binary form must reproduce the above copyright" [:br]
        "notice, this list of conditions and the following disclaimer in the" [:br]
        "documentation and/or other materials provided with the distribution." [:br]
        [:br]
        "3. Neither the name of the copyright holder nor the names of its" [:br]
        "contributors may be used to endorse or promote products derived from" [:br]
        "this software without specific prior written permission." [:br]
        "" [:br]
        "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS" [:br]
        "IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED" [:br]
        "TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A" [:br]
        "PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT" [:br]
        "HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL," [:br]
        "SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT" [:br]
        "LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE," [:br]
        "DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY" [:br]
        "THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT" [:br]
        "(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE" [:br]
        "OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." [:br]]]
    [:section {:class "two columns"}]])
