(ns edu.unc.cscc.views.layout.datasets
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.layout.nav :as nav]
            [edu.unc.cscc.views.routes :as routes]
            [edu.unc.cscc.views.layout.edit-dataset :as edit-dataset]
            [edu.unc.cscc.views.layout.controls :as c]
            [edu.unc.cscc.views.layout.pager :refer [pager]]))


(defn listing
  [{:keys [on-select]}]
  (let [project-id (subscribe [:view-state :project-id])
        term (r/atom "")
        page (r/atom 1)
        page-size 10
        ; FIXME - this will fail if project-id isn't available via view-state immediately
        params (r/atom {:project-id @project-id
                        :term "" :sort-by :identifier
                        :descending? true :offset 0 :limit page-size
                        ; only load ACLs if we're going to need them to figure
                        ; out whether to display the edit link
                        :with-user-acl? (nil? on-select)})
        search-results (subscribe [:datasets/search] [params])
        user (subscribe [:current-user])
        project (subscribe [:projects] [project-id])
        project-acl (subscribe [:projects/acl] [project-id])
        exec-search
          (fn [term]
            (reset! page 1)
            (swap! params assoc :term term :offset 0))]
    (fn [{:keys [show-new-button? filter]
          :or {show-new-button? false
              filter #(not (nil? %))}
          :as listing-args}]
      (let [datasets
            (if (not (keyword? @search-results))
              (clojure.core/filter filter (:results @search-results))
              @search-results)
            show-new-button? (and show-new-button? (auth/acl-allows? (:id @user) @project-acl :create-datasets))
            dataset-acls (:acls @search-results)
            total-count (:total-count @search-results)]
        [:div
          (if (or (not (map? @project)) (not (map? @project-acl)) (= :loading datasets))
            [:p "Loading..."]
            [:div
              {:style {:padding 0}}
             [:section {:id "dataset-header" :class "clearfix"}
                (when show-new-button?
                  [:button {:on-click #(do (routes/go! (routes/dataset {:id "new" :project-id (:id @project)} )))
                           :class "no-mobile-hd"
                           :style
                           {:margin 0
                             :float :left
                            :margin-right "1rem"}}
                  "new dataset"])
                (when show-new-button?
                  [:button {:on-click #(do (routes/go! (routes/dataset {:id "new" :project-id (:id @project)} )))
                             :class "yes-mobile-hd"
                            :style
                            {:margin 0
                              :float :left
                             :margin-right "1rem"}}
                   "new"])
              [:section {:id "dataset-search"
                         :class "text-right"
                          :style {:float :right}}
                [:input {:type "search"
                          :placeholder "search"
                          :value @term
                          :on-change #(reset! term (-> % .-target .-value))
                          :on-key-down
                            (fn [e]
                              (when (= 13 (.-keyCode e))
                                (exec-search @term)))}]
                  [:button
                    {:class "no-mobile-hd"
                      :on-click #(exec-search @term)
                      :style {:margin-right 0 :margin-left "1rem"}
                      :disabled (= (:term @params) @term)}
                    "search"]
                  [:button {:class "yes-mobile-hd" :style {:margin-right 0 :margin-left "1rem" :padding "0 0.5rem"}}
                    "🔎"]]]
             [:section {:id "datasets"
                        :style {:margin-top "1rem"}}
              (if (not (= :loading datasets))
                (if (= :error datasets)
                  [:p {:class "error"} "Error loading datasets"]
                  (if (empty? datasets)
                    [:div
                      [:div "No datasets found. "
                        (if (and (empty? (:term @params)) show-new-button?) ; TODO - add perms
                          [:span "Would you like to "
                            [:a {:href (routes/dataset {:project-id (:id @project) :id "new"})} "create one?"]])]]
                    [:div
                      [:table {:class "sortable" :style {:border-collapse :initial :width "100%"}}
                        [:thead [:tr
                                  [:th {:on-click
                                        (fn []
                                          (when (= :identifier (:sort-by @params))
                                            (swap! params assoc :descending? (not (:descending? @params))))
                                          (swap! params assoc :sort-by :identifier))
                                        :class (str/join " " [(if (= (:sort-by @params) :identifier) "selected")
                                                              (if (:descending? @params) "desc" "asc")])}
                                  "identifier"]
                                  [:th {:on-click
                                          (fn []
                                            (when (= :description (:sort-by @params))
                                              (swap! params assoc :descending? (not (:descending? @params))))
                                            (swap! params assoc :sort-by :description))
                                        :class (str/join " " [(if (= (:sort-by @params) :description) "selected")
                                                              (if (:descending? @params) "desc" "asc")])}
                                  "description"]
                                  [:th {:on-click
                                          (fn []
                                            (when (= :content-type (:sort-by @params))
                                              (swap! params assoc :descending? (not (:descending? @params))))
                                            (swap! params assoc :sort-by :content-type))
                                        :class (str/join " " [(if (= (:sort-by @params) :content-type) "selected")
                                                              (if (:descending? @params) "desc" "asc")])}
                                  "type"]]]
                        (into [:tbody]
                          (for [dataset datasets]
                            [:tr
                              [:td
                                (if (:on-select listing-args)
                                  [:a
                                  {:href "javascript:void(0)"
                                    :on-click #(on-select dataset)}
                                    (:identifier dataset)]
                                  ; on-select wasn't provided, so we will only bind
                                  ; handlers for the datasets that the user can modify
                                  (if (auth/acl-allows? (:id @user) (get dataset-acls (:id dataset)) :write)
                                    [:a
                                    {:href "javascript:void(0)"
                                      :on-click
                                      #(routes/go!
                                        (routes/dataset
                                          {:id (:id dataset)
                                            :project-id (:id (:project dataset))}))}
                                      (:identifier dataset)]
                                    [:span (:identifier dataset)])
                                  )]
                              [:td (:description dataset)]
                              [:td (:content-type dataset)]]))
                        ]
                    [:div {:style {:text-align :center}}
                      [pager
                        {:page-size page-size :item-count (:total-count @search-results) :start-page @page 
                          :loading? (= :loading @search-results)
                          :page-fn
                            (fn [new-page]
                              (reset! page new-page)
                              (swap! params assoc :offset (* (dec new-page) page-size))) }]]
                     ])))
              ]])]))))


(defn component
  []
  (let [view-state (subscribe [:view-state])]
    (fn []
      [:div {:class "eight columns"}
        (if (not (:id @view-state))
          [listing {:show-new-button? true}]
          [edit-dataset/component (:id @view-state)])])))
