(ns edu.unc.cscc.views.layout.edit-dataset
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.layout.acl :as acl]
            [edu.unc.cscc.views.remote-api :as remote]
            [edu.unc.cscc.views.routes :as routes]))

; TODO - modify dataset atom instead of copy all values out individually
(defn edit-dataset
  [existing chart-ids acl project-id project-acl]
  (let [new? (empty? existing)
        identifier (r/atom (get existing :identifier ""))
        user (subscribe [:current-user])
        taken-identifiers (r/atom [])
        description (r/atom (get existing :description ""))
        acl (r/atom acl)
        error (r/atom nil)
        js-file (r/atom nil)
        submitted? (r/atom false)
        can-delete? (r/atom false)
        deleted? (r/atom false)
        can-edit-acl? (auth/acl-allows? (:id @user) project-acl :manage-dataset-access-control)]
    (fn [existing chart-ids _ project-id project-acl]
      [:div {:id "new-entity" :class "row"}
        [:div
          [:h3
            (if new?
              "New Dataset"
              (str "Edit Dataset: " @identifier))]
          [:div {:class "ten columns"}
            [:fieldset
              [:label {:class ""}
                [:input
                  {:type :text
                    :placeholder "unique-identifier"
                    :on-change
                      (fn [e]
                        (reset! identifier (-> e .-target .-value)))
                    :value @identifier}] ]
              [:label {:class ""}
              [:textarea {:placeholder
                          "This is where you can put a friendly description of the dataset. Feel free to describe it as much or as little as you'd like."
                          :value @description
                          :on-change #(reset! description (-> % .-target .-value))}]]]
            [:fieldset {:class "clearfix"}
              [:div {:style {:float :left :margin-right "0.5rem"}}
                (if (not (empty? chart-ids))
                  [:p
                    {:class "small" :style {:margin 0 :margin-bottom "0.25rem"}}
                    "This dataset cannot be deleted.  It is in use by " (count chart-ids) " chart(s)."]
                  (if (not new?)
                    [:button
                      {:style
                          {:float :left
                            :margin-right "0.5rem"
                            :margin-bottom "0.5rem"
                            :background-color (if @can-delete? :red :inherit)
                            :color (if @can-delete? :white :black)}
                        :disabled (or @deleted? @submitted?)
                        :on-click
                        (fn []
                          (if @can-delete?
                            (do
                              (reset! deleted? true)
                              (remote/dataset-delete
                                {:id (:id existing)
                                  :on-success #(routes/go! (routes/datasets {:project-id project-id}))
                                  :on-failure #(reset! error "Error deleting dataset")}))
                            (swap! can-delete? not))) }
                      (if @deleted?
                        "deleting..."
                        (if @can-delete? "! DELETE !" "delete"))]))
                [:label
                  {:class "file-input" :style {:float :left}}
                  [:span
                    {:class "contents"
                      :style {:margin-left 0}}
                    (if (some? @js-file)
                      [:span [:b "Dataset: "] (.-name @js-file)]
                      (if new? "select dataset" "replace dataset"))]
                  [:input {:type "file"
                            :on-change (fn [evt]
                                         (let [file (aget (-> evt .-target .-files) 0)]
                                           (reset! js-file file)))}]
                  [:span {:class "filename"}]]]]

            [:fieldset {:class "clearfix"}
              [:div {:style {:float :left :margin-right "0.5rem"}}
                [:button
                  {:style {:float :left  :margin-right "0.5rem" :margin-bottom "0.5rem"}
                    :on-click #(routes/go! (routes/datasets {:project-id project-id}))}
                  "cancel"]]
              [:button
                {:style {:float :right}
                  :disabled
                  (or
                    (empty? @identifier)
                    (and new? (nil? @js-file))
                    @submitted?
                    @deleted?
                    (some #(= @identifier %) @taken-identifiers))
                  :on-click
                    (fn []
                      (reset! error nil)
                      (reset! submitted? true)
                      (remote/dataset-save
                        {:id (if (not new?) (:id existing))
                          :project-id project-id
                          :identifier @identifier
                          :description @description
                          :js-file @js-file
                          :on-success
                            (fn [saved-ds]
                              (let [on-success
                                    #(routes/go!
                                      (routes/datasets {:project-id project-id}))]
                                (if can-edit-acl?
                                  (remote/dataset-acl-save
                                    {:id (:id saved-ds)
                                      ; if it's a new dataset, then the ACL update is
                                      ; "partial" so that the user can't lock themselves
                                      ; out.
                                      :acl (if new? (assoc @acl :partial true) @acl)
                                      :on-success on-success
                                      :on-failure
                                        (fn []
                                          (reset! error "Error saving dataset ACL")
                                          (reset! submitted? false))})
                                  (on-success))))
                          :on-failure
                            (fn []
                              (reset! error "Error uploading dataset")
                              (reset! submitted? false))
                          :on-duplicate-identifier
                            (fn [identifier]
                              (swap! taken-identifiers conj identifier)
                              (reset! submitted? false))})
                      )}
                (if @submitted? "saving..." "save")]
              [:div {:style {:float :right}}
                [:p
                  {:class "text-right" :style {:color :red}}
                  (if @error
                    @error
                    (if (some #(= @identifier %) @taken-identifiers)
                      "Oops.  That identifier is already in use."))]]]
            (when can-edit-acl?
              [:div {:style {:margin-top "1rem"}}
                [:h4 {:style {:margin 0}} "permissions"]
                (if (map? @user)
                  [acl/component @acl
                    {:read "Read Dataset" :write "Modify Dataset"}
                    #(reset! acl %)
                    (when new? #(not (= (:id @user) (:id %))))])])]
          ]])))

(defn component
  [id]
  (let [ds (if (number? id) (subscribe [:datasets id]) (r/atom {}))
        chart-ids (if (number? id) (subscribe [:datasets/chart-ids id]) (r/atom []))
        acl (if (number? id) (subscribe [:datasets/acl id]) (r/atom {}))
        user (subscribe [:current-user])
        project-id (subscribe [:view-state :project-id])
        project-acl (subscribe [:projects/acl-for-user (:id @user)] [project-id])]
    ; FIXME check for DS -> project mismatch w/ view state
    (fn [id]
      [:div
        (if
          (or (= :loading @ds) (= :loading @acl)
            (= :loading @project-acl) (= :loading @chart-ids))
          [:div "Loading..."]
          (if (= :new id)
            [edit-dataset nil @chart-ids @acl @project-id @project-acl]
            [edit-dataset @ds @chart-ids @acl @project-id @project-acl])) ])))
