(ns edu.unc.cscc.views.layout.login
  (:require
    [reagent.core :as r]
    [re-frame.core :refer [dispatch
                            dispatch-sync
                            subscribe]]
    [edu.unc.cscc.views.config :as config]
    [edu.unc.cscc.views.auth0 :as auth0]
    [edu.unc.cscc.views.components.modal :as modal]
    [shodan.console :as console]))

(defn auth0-logo []
  [:svg {:xmlns "http://www.w3.org/2000/svg" :viewBox "0 0 64 64" :height 20}
   [:path {:d "M49.012 51.774L42.514 32l17.008-12.22h-21.02L32.005 0h21.032l6.506 19.78c3.767 11.468-.118 24.52-10.53 31.993zm-34.023 0L31.998 64l17.015-12.226-17.008-12.22zm-10.516-32c-3.976 12.1.64 24.917 10.5 32.007v-.007L21.482 32 4.474 19.774l21.025.007L31.998 0H10.972z", :fill "#eb5424"}]])

(defn component []
  (let [username (r/atom "")
        password (r/atom "")
        credential-source (r/atom "local")
        corrected (r/atom false) ; whether the user tried to correct credentials
        login-state (subscribe [:view-state])
        load-error (subscribe [:load-error])
        credential-sources (subscribe [:credential-sources])]
    (fn []
      [:div {:id "login" :style {:min-width "30rem"}}
       [:img {:src "images/dash-icon.png" :id "dash-icon"}]
       [:h1 {:class "text-center"} "Welcome." ]
       [:form
        {:style {:max-width "30rem" :margin "0 auto" :padding "2rem"}}
        [:div.auth0-container
         [:button.button.auth0-button {:onClick auth0/login-with-redirect :type "button"}
          [auth0-logo]
          "Login with Auth0"]
         (when (= :auth0-failure @load-error)
           [:div {:class "pw-error"}
            "Failed to communicate with Auth0 (user does not have access to Views)"])
         [:p "OR"]]
        [:fieldset
         [:h3.cssc-sources-title "CSSC Credential Sources"]
         [:legend ""]
         [:label {:class ""}
          [:input {:style {:width "100%"}
                    :type "text"
                   :placeholder "username"
                   :name "username"
                   :value @username
                   :on-change (fn [e]
                                (reset! username (-> e .-target .-value))
                                (reset! corrected true))}]]
         [:label {:class ""}
          [:input {:style {:width "100%"}
                   :type "password"
                   :placeholder "password"
                   :name "password"
                   :value @password
                   :on-change (fn [e]
                                (reset! password (-> e .-target .-value))
                                (reset! corrected true))}]]
          [:label {:class ""}
            (if (= :loading @credential-sources)
              [:p "Loading..."]
              (if (= :error @credential-sources)
                [:p {:class "error"} "Failed to load credential sources from server"]
                [:div {:class "text-right"}
                  [:span "account: "]
                  [:select
                    {:style {:max-width "20rem"}
                      :value @credential-source
                      :on-change #(reset! credential-source (-> % .-target .-value))}
                    (map
                      (fn [src]
                        [:option
                          {:key (:identifier src) :value (:identifier src)}
                          (:name src) " - " (:description src)])
                      (vals @credential-sources))]]))]
          [:div {:id "submit-container"}
           [:button.button {:style {:margin-right 0}
                     :on-click
                     (fn [e]
                       (.preventDefault e)
                       (reset! corrected false)
                       (dispatch [:user-login @username @password @credential-source])
                       false)
                     :disabled (or (empty? @username)
                                    (empty? @password)
                                    (= :error @credential-sources))}
            "Log in"]]
         [:div
          ]
         ; TODO progress wheel
         [:div {:class "pw-error"}
          (cond
            (and (not @corrected) (= :rejected @login-state))
            "Username or password is incorrect"
            (and (not @corrected) (= :failure @login-state))
            "Failed to communicate with server"
            true
            [:br])]

         ]]])))
