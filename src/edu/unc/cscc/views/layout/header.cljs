(ns edu.unc.cscc.views.layout.header
  (:require [reagent.core :as r]
            [re-frame.core :refer [dispatch
                                   dispatch-sync
                                   subscribe]]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.config :refer [product-name]]
            [edu.unc.cscc.views.routes :as routes]))

(defn component []
  (let [user (subscribe [:current-user])]
    (fn []
      (if @user
        [:header {:class "row"}
         [:div {:class "seven columns"}
          ; logo
          [:div {:class "head-logo-wrapper"}
           [:a {:href "#"}
            [:img {:src "images/dash-icon.png"}]
            [:h1 product-name]]
           [:p {:class "motto"} "data at a glance"]]]
         [:div {:id "username-container"
                :class "five columns u-text-right"}
          [:span {:id "username"}
            [:a {:href (routes/user {:id (:id @user)})}
              (if (or (empty? (:name @user)) (= (:username @user) (:name @user)))
                (:username @user)
                (str (:name @user) " (" (:username @user) ")"))]]
          [:button {:id "logout"
                    :on-click #(routes/go! (routes/logout))}
           "logout"]]]
        ; unauthenticated
        [:div]))))
