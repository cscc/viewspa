(ns edu.unc.cscc.views.layout.pager
    (:require [shodan.console :as console]))

(defn- max-pages
    [page-size count]
    (max 1 
        ((if (< 0 (mod count page-size))
            inc
            identity) (int (/ count page-size)))))

(defn pager
    [_]
        (fn [{:keys [page-size start-page item-count page-fn disabled? loading?]
                :or [start-page (if (and (number? page-size) (< 0 page-size)) page-size 9) 
                        disabled? false 
                        loading? false]}]
            (let [total-pages (if loading? "?" (max-pages page-size item-count))]
                (console/log total-pages)
                [:div
                    {:style {:margin "0.5rem 0"}}
                    
                    ; page down
                    [:button
                        {:on-click #(page-fn (dec start-page))
                            :disabled (or (<= start-page 1) disabled? loading?)} "<"]

                    (when loading? "loading ") "page " start-page " of " total-pages
                    ; page sel dropdown


                    ; page up
                    [:button
                        {:on-click #(page-fn (inc start-page))
                            :disabled (or (>= start-page total-pages) disabled? loading?)} ">"]
                    
                    ])))