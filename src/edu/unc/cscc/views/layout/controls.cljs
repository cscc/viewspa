(ns edu.unc.cscc.views.layout.controls
  (:require
    [clojure.string :as str]
    [reagent.core :as r]
    [shodan.console :as console]))

(defn scale-out
  ([number-of-bars]
    (scale-out number-of-bars :small))
  ([number-of-bars size]
    (assert (some #(= size) [:small :normal :large]))
    (fn []
      (into [:div {:class
                   (str/join " " ["la-line-scale-pulse-out" "la-dark"
                                  (condp = size
                                    :small "la-sm"
                                    :normal ""
                                    :large "la-2x")])
                   :style {:display "inline-block"}}]
            (repeat number-of-bars [:div])))))

(defn- toggle-element
  [bar-id {:keys [name on-select selected?]}]
  (let [id (str "toggle-bar-element-" (.random js/Math))]
    [:div {:class "option-wrapper"}
      [:input
        {:type "radio"
          :id id
          :name bar-id
          :value id
          :checked (if selected? :checked)
          :read-only true
          :on-click on-select }]
        [:label {:for id}
          [:div {:class "label-inner"} name]]]))

(defn toggle-bar
  [& options]
  (let [bar-id (str "toggle-bar-" (.random js/Math))]
    (into [:div {:class "toggle-bar noselect"}]
      (for [o options]
        ^{:key o}
        [toggle-element bar-id o]))))


(defn- combo-element
  [bar-id {:keys [name on-click selected?]}]
  (let [id (str "combo-bar-element-" (.random js/Math))
        value (r/atom selected?)]
    (fn [bar-id {:keys [name on-click selected?] :or {on-click #()}}]
      [:div {:class "option-wrapper"}
        [:input
          {:type "checkbox"
            :id id
            :name bar-id
            :value id
            :checked (if @value :checked)
            :read-only true
            :on-click
              (fn []
                (swap! value not)
                (on-click @value))}]
        [:label {:for id}
          [:div {:class "label-inner"}
            name]]])))

(defn combo-bar
  [& options]
  (let [bar-id (str "combo-bar-" (.random js/Math))]
    (into [:div {:class "combo-bar noselect"}]
      (for [o options]
        ^{:key o}
        [combo-element bar-id o]))))
