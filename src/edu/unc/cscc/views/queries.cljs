(ns edu.unc.cscc.views.queries
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require
    [re-frame.core :refer [register-handler
                           path
                           reg-sub-raw
                           dispatch
                           dispatch-sync
                           subscribe]]
    [reagent.ratom :refer [make-reaction]]
    [shodan.console :as console]

    [edu.unc.cscc.views.auth :as auth]
    [edu.unc.cscc.views.remote-api :as remote]))

; hack to get us onto re-frame 0.8.0 faster
(def register-sub reg-sub-raw)


;;;
;;;
;;; TODO - lots of boilerplate copy/paste here.  Macros, or at least some
;;; cleaner fns please
;;;
;;;



;;;;; authentication

(register-sub
  :credential-sources
  (fn [db, [sid]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:credential-sources] :unknown)]
          (if (not (or (map? res) (keyword? res)))
            (do
              (auth/get-credential-sources
                {:on-success
                    (fn [sources]
                      (let [s (zipmap (map #(:identifier %) sources) sources)]
                        (dispatch [:set [:credential-sources] s])))
                  :on-failure #(dispatch [:set [:credential-sources] :error])})
              :loading)
            res)))
      :on-dispose #(dispatch [:unset [:credential-sources]])) ))

;;;;; user queries


(register-sub
  :jwt-token
  (fn [db, [sid]]
    (reaction (get-in @db [:user :token]))))

(register-sub
  :current-user
  (fn [db]
    (reaction (get-in @db [:user :entity]))))

(register-sub
  :current-user-id
  (fn [db]
    (reaction (get-in @db [:user :entity :id]))))


;;;;; VIEW stuff

(register-sub
  :active-view
  (fn [db, [sid]]
    (assert (= sid :active-view))
    (reaction (get-in @db [:view :name]))))

(register-sub
  :view-state
  (fn [db [_ & path]]
    (reaction (get-in @db (concat [:view :state] path)))))

(register-sub
 :load-error
 (fn [db [_]]
   (reaction (get @db :load-error))))

(register-sub
  :charts
  (fn [db [_ id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:charts :entities id] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown res)
            (if user
              (do
                (remote/chart-load
                  {:id id
                    :on-success #(dispatch [:set [:charts :entities id] %])
                    :on-failure #(dispatch [:set [:charts :entities id] :error])})
                :loading)
              nil)
            res)))
      :on-dispose #(dispatch [:unset [:charts :entities id]])) ))

(register-sub
  :charts/search
  (fn [db _ [{:keys [project-id term with-user-acl?] :as params}]]
    (make-reaction
      (fn []
        (let [results (get-in @db [:charts :search params] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown results)
            (if user
              (do
                (remote/chart-search
                  (merge params
                    {:on-success #(dispatch [:set [:charts :search params] %])
                      :on-failure #(dispatch [:set [:charts :search params] :error])}))
                :loading)
              nil)
            results)))
      :on-dispose
        (fn []
          (dispatch [:unset [:charts :search params]])))
    ))


(register-sub
  :charts/acl
  (fn [db [_ id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:charts :acl id] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown res)
            (if user
              (do
                (remote/chart-acl-load
                  {:id id
                    :on-success #(dispatch [:set [:charts :acl id] %])
                    :on-failure #(dispatch [:set [:charts :acl id] :error])})
                :loading)
              nil)
            res)))
      :on-dispose #(dispatch [:unset [:charts :acl id]])) ))

(register-sub
  :datasets
  (fn [db [_ id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:datasets :entities id] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown res)
            (if user
              (do
                (remote/dataset-load
                  {:id id
                    :on-success #(dispatch [:set [:datasets :entities id] %])
                    :on-failure #(dispatch [:set [:datasets :entities id] :error])})
                :loading)
              nil)
            res)))
      :on-dispose #(dispatch [:unset [:datasets :entities id]])) ))


(register-sub
  :datasets/data
  (fn [db [_ id] [dyn-id]]
    (let [id (if id id dyn-id)]
      (make-reaction
        (fn []
          (let [res (get-in @db [:datasets :data id] :unknown)
                user (get-in @db [:user :entity])]
            (if (= :unknown res)
              (if user
                (do
                  (remote/dataset-load-data
                    {:id id
                      :on-success #(dispatch [:set [:datasets :data id] %])
                      :on-failure #(dispatch [:set [:datasets :data id] :error])})
                  :loading)
                nil)
              res)))
        :on-dispose #(dispatch [:unset [:datasets :data id]]))) ))

(register-sub
  :datasets/acl
  (fn [db [_ id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:datasets :acl id] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown res)
            (if user
              (do
                (remote/dataset-acl-load
                  {:id id
                    :on-success #(dispatch [:set [:datasets :acl id] %])
                    :on-failure #(dispatch [:set [:datasets :acl id] :error])})
                :loading)
              nil)
            res)))
      :on-dispose #(dispatch [:unset [:datasets :acl id]])) ))

(register-sub
  :datasets/chart-ids
  (fn [db [_ dataset-id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:datasets :chart-ids dataset-id] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown res)
            (if user
              (do
                (remote/dataset-find-chart-ids
                  {:id dataset-id
                    :on-success #(dispatch [:set [:datasets :chart-ids dataset-id] %])
                    :on-failure #(dispatch [:set [:datasets :chart-ids dataset-id] :error])})
                :loading)
              nil)
            res)))
      :on-dispose #(dispatch [:unset [:datasets :chart-ids dataset-id]])) ))

(register-sub
  :datasets/search
  (fn [db _ [{:keys [project-id term sort-by offset limit descending? with-user-acl?] :as p}]]
    (make-reaction
      (fn []
        (let [results (get-in @db [:datasets :search p] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown results)
            (if user
              (do
                (remote/dataset-search
                  (merge p
                    {:on-success #(do
                                    (dispatch [:set [:datasets :search p] %]))
                      :on-failure #(dispatch [:set [:datasets :search p] :error])}))
                :loading)
              nil)
            results)))
      :on-dispose
        (fn []
          (dispatch [:unset [:datasets :search p]]))) ))

;;;;;; DASHBOARDS

(register-sub
  :dashboards
  (fn [db [_ id] [dyn-id]]
    (let [id (if id id dyn-id)]
      (make-reaction
        (fn []
          (let [res (get-in @db [:dashboards :entities id] :unknown)
                user (get-in @db [:user :entity])]
            (if (= :unknown res)
              (if user
                (do
                  (remote/dashboard-load
                    {:id id
                      :on-success #(dispatch [:set [:dashboards :entities id] %])
                      :on-failure #(dispatch [:set [:dashboards :entities id] :error])})
                  :loading)
                nil)
              res)))
        :on-dispose #(dispatch [:unset [:dashboards :entities id]]))) ))

(register-sub
  :dashboards/search
  (fn [db _ [{:keys [project-id term sort-by offset limit descending? with-user-acl?] :as p}]]
    (make-reaction
      (fn []
        (let [results (get-in @db [:dashboards :search p] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown results)
            (if user
              (do
                (remote/dashboard-search
                  (merge p
                    {:on-success #(do
                                    (dispatch [:set [:dashboards :search p] %]))
                      :on-failure #(dispatch [:set [:dashboards :search p] :error])}))
                :loading)
              nil)
            results)))
      :on-dispose
        (fn []
          (dispatch [:unset [:dashboards :search p]]))) ))


;;;;;; PROJECTS
(register-sub
  :projects/search
  (fn [db _ [{:keys [term with-user-acl? offset limit] :as p}]]
    (make-reaction
      (fn []
        (let [results (get-in @db [:projects :search p] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown results)
            (if user
              (do
                (remote/project-search
                  (merge p 
                    {:on-success #(dispatch [:set [:projects :search p] %])
                      :on-failure #(dispatch [:set [:projects :search p] :error])}))
                :loading)
              nil)
            results)))
      :on-dispose
        (fn []
          (dispatch [:unset [:projects :search p]])))))

(register-sub
  :projects
  (fn [db [_ id] [dyn-id]]
    (let [id (if dyn-id dyn-id id)]
      (make-reaction
        (fn []
          (if (not id)
            nil
            (let [res (get-in @db [:projects :entities id] :unknown)
                  user (get-in @db [:user :entity])]
              (if (= :unknown res)
                (if user
                  (do
                    (remote/project-load
                      {:id id
                        :on-success #(dispatch [:set [:projects :entities id] %])
                        :on-failure #(dispatch [:set [:projects :entities id] :error])})
                    :loading)
                  nil)
                res))))
        :on-dispose #(dispatch [:unset [:projects :entities id]]))) ))

(register-sub
  :projects/acl
  (fn [db [_ id] [dyn-id]]
    (let [id (if dyn-id dyn-id id)]
      (make-reaction
        (fn []
          (if (not id)
            nil
            (let [res (get-in @db [:projects :acl id] :unknown)
                  user (get-in @db [:user :entity])]
              (if (= :unknown res)
                (if user
                  (do
                    (remote/project-acl-load
                      {:id id
                        :on-success #(dispatch [:set [:projects :acl id] %])
                        :on-failure #(dispatch [:set [:projects :acl id] :error])})
                    :loading)
                  nil)
                res))))
        :on-dispose #(dispatch [:unset [:projects :acl id]]))) ))

(register-sub
  :projects/acl-for-user
  (fn [db [_ user-id id] [dyn-id]]
    (assert (number? user-id))
    (let [id (if dyn-id dyn-id id)
          key {:id id :user-id user-id}]
      (make-reaction
        (fn []
          (if (not id)
            nil
            (let [res (get-in @db [:projects :acl key] :unknown)
                  user (get-in @db [:user :entity])]
              (if (= :unknown res)
                (if user
                  (do
                    (remote/project-acl-load
                      {:id id
                        :user-id user-id
                        :on-success #(dispatch [:set [:projects :acl key] %])
                        :on-failure #(dispatch [:set [:projects :acl key] :error])})
                    :loading)
                  nil)
                res))))
        :on-dispose #(dispatch [:unset [:projects :acl key]]))) ))


;;;;;; RESOURCES

(register-sub
  :resources/for-chart
  (fn [db [_ chart-id path]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:resources :entities chart-id path] :unknown)]
          (if (= :unknown res)
            (do
              (remote/resource-load
                {:chart-id chart-id
                  :path path
                  :on-success #(dispatch [:set [:resources :entities chart-id path] %])
                  :on-failure #(dispatch [:set [:resources :entities chart-id path] :error])})
              :loading)
            res)))
      :on-dispose #(dispatch [:unset [:resources :entities chart-id path]])) ))

;;;;;; USERS

(register-sub
  :users
  (fn [db [_ id]]
    (make-reaction
      (fn []
        (let [res (get-in @db [:users :entities id] :unknown)]
          (if (= :unknown res)
            (do
              (remote/user-load id
                  #(dispatch [:set [:users :entities id] %])
                  #(dispatch [:set [:users :entities id] :error]))
              :loading)
            res)))
      :on-dispose #(dispatch [:unset [:users :entities id]])) ))

(register-sub
  :users/search
  (fn [db _ [{:keys [term limit offset] :or {limit 10000 offset 0} :as p}]]
    (make-reaction
      (fn []
        (let [results (get-in @db [:users :search p] :unknown)
              user (get-in @db [:user :entity])]
          (if (= :unknown results)
            (if user
              (do
                (remote/user-search
                  {:query term
                    :limit limit
                    :offset offset
                    :on-success #(dispatch [:set [:users :search p] %])
                    :on-failure #(dispatch [:set [:users :search p] :error])})
                :loading)
              nil)
            results)))
      :on-dispose
        (fn []
          (dispatch [:unset [:users :search p]])))
    ))
