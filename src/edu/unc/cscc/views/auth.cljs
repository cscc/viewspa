(ns edu.unc.cscc.views.auth
  (:require
    [ajax.core :refer [GET POST]]
    [re-frame.core :refer [subscribe dispatch dispatch-sync]]
    [shodan.console :as console :include-macros true]

    [edu.unc.cscc.views.config :as config]
    [edu.unc.cscc.views.utils.urls :as url-utils]
    [edu.unc.cscc.views.remote-api :as remote]))

; TODO - the role stuff is messy.  It grew from one or two of them
; and should probably be rewritten to use a generic has-role?/set-role pair of
; fns.  This will have to happen later when we have more time.

(defn set-manage-users
  [user granted?]
  (if granted?
    (assoc user :roles (conj (:roles user) "manage_users"))
    (assoc user :roles (remove #(= "manage_users" %) (:roles user)))))

(defn set-create-projects
  [user granted?]
  (if granted?
    (assoc user :roles (conj (:roles user) "create_projects"))
    (assoc user :roles (remove #(= "create_projects" %) (:roles user)))))

(defn set-create-dashboards
  [user granted?]
  (if granted?
    (assoc user :roles (conj (:roles user) "create_dashboards"))
    (assoc user :roles (remove #(= "create_dashboards" %) (:roles user)))))

(defn set-manage-project-permissions
  [user granted?]
  (if granted?
    (assoc user :roles (conj (:roles user) "manage_project_acls"))
    (assoc user :roles (remove #(= "manage_project_acls" %) (:roles user)))))

(defn is-superuser?
  [user]
  (and
    (or (seq? (:roles user)) (vector? (:roles user)))
    (some #(= "superuser" %) (:roles user))))

(defn can-manage-users?
  [user]
  (and
    (or (seq? (:roles user)) (vector? (:roles user)))
    (some #(= "manage_users" %) (:roles user))))

(defn can-create-projects?
  [user]
  (and
    (or (seq? (:roles user)) (vector? (:roles user)))
    (some #(= "create_projects" %) (:roles user))))

(defn can-create-dashboards?
  [user]
  (and
    (or (seq? (:roles user)) (vector? (:roles user)))
    (some #(= "create_dashboards" %) (:roles user))))

(defn can-manage-project-permissions?
  [user]
  (and
    (or (seq? (:roles user)) (vector? (:roles user)))
    (some #(= "manage_project_acls" %) (:roles user))))

;;;;; ACLs

(defn acl-allows?
  ([user-id acl permission]
    (acl-allows? user-id acl permission true))
  ([user-id acl permission include-inherited?]
    (assert
      (or (number? user-id) (= :anonymous user-id) (= :all-users user-id))
      "user-id must be number, :anonymous, or :all-users")
    (condp = user-id
      :anonymous
        (true? (get-in acl [:anonymous permission] false))
      :all-users
        (or
          (true? (get-in acl [:all-users permission] false))
          (and include-inherited? (true? (get acl [:anonymous permission] false))))
      (or
        (true? (get-in acl [:user-entries user-id :entries permission] false))
        (and include-inherited?
          (or
            (true? (get-in acl [:all-users permission] false))
            (true? (get-in acl [:anonymous permission] false))))))))

(defn- private-acl-modify
  [user-id acl permission grant?]
  (assert
    (or (number? user-id) (= :anonymous user-id) (= :all-users user-id))
    "user-id must be number, :anonymous, or :all-users")
  (assert (keyword? permission) "permission must be keyword")
  (condp = user-id
    :anonymous
      (if (true? grant?)
        (assoc-in acl [:anonymous permission] (true? grant?))
        (update-in acl [:anonymous] dissoc permission))
    :all-users
      (if (true? grant?)
        (assoc-in acl [:all-users permission] (true? grant?))
        (update-in acl [:all-users] dissoc permission))
    (if (true? grant?)
      (assoc-in acl [:user-entries user-id :entries permission] true)
      (update-in acl [:user-entries user-id :entries] dissoc permission))))

(defn acl-grant
  "Grant the given permission to the specified user in the given ACL.  user-id
  must be number or :anonymous or :all-users.  permission must be keyword"
  [user-id acl permission]
  (private-acl-modify user-id acl permission true))

(defn acl-deny
  "Deny the given permission to the specified user in the given ACL.  user-id
  must be number or :anonymous or :all-users.  permission must be keyword"
  [user-id acl permission]
  (private-acl-modify user-id acl permission false))

(defn acl-add
  "Add the given user to the given ACL, assigning them the permissions in the given
  map (key is permission, value true/false for grant/deny)"
  [user acl permissions]
  (assert (number? (:id user)) "User lacks ID")
  (assoc-in acl [:user-entries (:id user)] {:user user :entries permissions}))

(defn acl-get-permissions
  "Get the permissions for the specified user in the given ACL.  user-id may be
  number id or :anonymous or :all-users Returns a map with permission names as
  keys, boolean for grant/deny as value or nil if user is not in ACL"
  [acl user-id]
  (assert
    (or (number? user-id) (= :anonymous user-id) (= :all-users user-id))
    "user-id must be number, :anonymous, or :all-users")
  (condp = user-id
    :anonymous (get acl :anonymous)
    :all-users (get acl :all-users)
    (get-in acl [:user-entries user-id :entries] nil)))

(defn acl-get-users
  "Get the users with entries the given ACL"
  [acl]
  (map #(:user %) (vals (:user-entries acl))))

(defn acl-represents?
  "Test whether the given acl represents the given user."
  [acl user]
  (contains? (:user-entries acl) (:id user)))


(defn acl-filter-users
  "Filter the user-entries of an ACL; resulting ACL's user entries will include 
  only entries for which (f user) returns true."
  [acl f]
  (update acl :user-entries
    (fn [entries]
      (into {}
        (filter (fn [[_ entry]] (f (:user entry))) entries)))) )


;;;;; remote API for authorization/authentication stuff.
;;;;; here instead of remote-api since these work differently re: security
;;;;; and auth headers than the rest of the API

(defn get-credential-sources
  [{:keys [on-success on-failure]}]
  (GET (str config/endpoint "/auth/sources")
    {:keywords? true
      :format :json
      :response-format :json
      :handler on-success
      :error-handler
        (fn []
          (console/error "failed to load credential sources")
          (on-failure))}))

(defn auth0-login
  [access-token handler error-handler]
  (let [path (str config/endpoint "/auth/auth0-login")]
    (GET path
         {:params {:accessToken access-token}
          :headers {:accept "application/jwt"}
          :handler handler
          :error-handler error-handler})))

(defn login
  [username password credential-source-id handler error-handler]
  (GET (str config/endpoint "/auth")
       {:params {:username username
                 :password password
                 :credential-source-id credential-source-id}
        :headers {:accept "application/jwt"}
        :handler
        (fn [token]
          (dispatch-sync [:set-jwt-token token])
          ; chain our update of current user info
          (remote/user-load :current
            #(handler %)
            #(error-handler %)))
        :error-handler error-handler}))
