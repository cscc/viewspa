(ns edu.unc.cscc.views.remote-api
  (:require
    [ajax.core :refer [GET POST PUT DELETE default-interceptors to-interceptor]]
    [clojure.string :as str]
    [re-frame.core :refer [subscribe dispatch]]
    [shodan.console :as console :include-macros true]

    [edu.unc.cscc.views.config :as config]
    ))

;;;;; housekeeping/setup

(defn- req-interceptor
  [request]
  (let [token (subscribe [:jwt-token])]
    (if (empty? @token)
      request
      (assoc-in request [:headers :Authorization] (str "Bearer " @token)))))

(defn initialize!
  []
  (reset! default-interceptors
    [(to-interceptor {:name "Token Interceptor"
                      :request req-interceptor})] ))

;;;;; utility

(defn- parse-integer
  "Parses the given string as a number, returning the fallback value if parsing
  failed for any reason."
  [str fallback]
  (if (re-find #"^\d+$" str)
    (.parseInt js/window str 10)
    fallback))



(defn keyword-key-map->number-key-map
  [map]
  (into {}
    (for [[k v] map]
      (do
        (assert (keyword? k) "key must be keywordized")
        [(parse-integer (name k) nil) v]))))

(defn parse-acl
  [acl]
  (update acl :user-entries keyword-key-map->number-key-map))

(defn parse-search-response
  [response]
  (update response :acls
    (fn [acls]
      (into {}
        (for [[user-id acl] (keyword-key-map->number-key-map acls)]
          [user-id (parse-acl acl)])))))

;;;;; CHARTS
(defn chart-search
  [{:keys [project-id term on-success on-failure with-user-acl? limit offset] :as params}]
  (assert (not (nil? project-id)) "missing project id")
  (GET (str config/endpoint "/projects/" project-id "/charts/search")
    {:params
      {:query term
        :limit limit
        :offset offset
        :with-user-acl (if with-user-acl? true false)}
      :response-format :json
      :keywords? true
      :handler (if with-user-acl? #(on-success (parse-search-response %)) on-success)
      :error-handler
        (fn [response]
          (console/error "chart search failed: " (str response))
          (on-failure) )}))


(defn chart-save
  [{:keys [project-id id name description main-resource-content required-datasets
            on-success on-failure on-duplicate-name]}]
  (assert (not (and id main-resource-content))
    "main resource cannot be provided for update")
  (assert project-id "missing project id")
  (assert on-success)
  (assert on-failure)
  (assert on-duplicate-name)
  ((if id PUT POST) (str config/endpoint "/projects/" project-id "/charts/" (when id id))
    {:format :json
      :response-format :json
      :keywords? true
      :params
        {:name name
          :description description
          :main-resource (if (not id) main-resource-content)
          :required-datasets required-datasets}
      :handler on-success
      :error-handler
        (fn [{:keys [status response]}]
          (if (= "name-exists" (:error response))
            (on-duplicate-name name)
            (do
              (console/error "Failed to save chart id " id " " status " - " (str response))
              (on-failure))))}))

(defn chart-load
  [{:keys [id on-success on-failure]}]
  (assert id "missing chart ID")
  (GET (str config/endpoint "/charts/" id )
    {:response-format :json
      :keywords? true
      :handler on-success
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to load chart id " id " status " status " response: " response)
          (on-failure)
          )}))

(defn chart-delete
  [{:keys [id on-success on-failure]}]
  (assert id "missing chart ID")
  (DELETE (str config/endpoint "/charts/" id)
    {:body nil
      :format :json
      :handler on-success
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to delete chart id " id " status " status " response: " response)
          (on-failure))}))

(defn chart-acl-load
  [{:keys [id on-success on-failure]}]
  (assert id "missing chart ID")
  (GET (str config/endpoint "/charts/" id "/acl" )
    {:response-format :json
      :keywords? true
      :handler
        (fn [acl]
          (on-success (parse-acl acl)))
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to load chart acl for id " id
            " status " status " response: " response)
          (on-failure))}))

(defn chart-acl-save
  [{:keys [id acl on-success on-failure]}]
  (assert id "missing chart ID")
  (assert acl "missing acl")
  (PUT (str config/endpoint "/charts/" id "/acl")
    {:params acl
      :format :json
      :handler on-success
      :error-handler
      (fn [{:keys [status response]}]
        (console/error
          "failed to save acl for chart id " id
          " status " status " response " response)
        (on-failure))}))


;;;;;; DATASETS
(defn dataset-search
  [{:keys [project-id term sort-by limit offset descending? on-success on-failure with-user-acl?]
    :or {term "" sort-by :identifier limit 20 offset 0 descending? true}
    :as p}]
  (assert
    (some #(= sort-by %) [:identifier :description :content-type])
    (str "sort-by must be one of [:identifier :description :content-type], was " sort-by))
  (assert (not (nil? project-id)) "missing project id")
  (GET (str config/endpoint "/projects/" project-id "/datasets/search")
    {:params
      {:query term
        :limit limit
        :offset offset
        :sort-by (name sort-by)
        :descending descending?
        :with-user-acl (true? with-user-acl?)}
      :format :json
      :response-format :json
      :keywords? true
      :handler (if with-user-acl? #(on-success (parse-search-response %)) on-success)
      :error-handler
        (fn [m]
          (console/error "dataset search failed: " (str m))
          (on-failure))} ))

(defn dataset-save
  [{:keys [id project-id identifier description js-file on-success on-failure on-duplicate-identifier]
    :or [on-duplicate-identifier on-failure]}]
  (assert (or (nil? id) (number? id)))
  (assert (number? project-id) "missing project id")
  (let [form-data (js/FormData.)]
    (when js-file (.append form-data "file" js-file))
    (.append form-data "identifier" identifier)
    (.append form-data "description" description)
    (if id
      ; update
      (PUT (str config/endpoint "/projects/" project-id "/datasets/" id)
        {:body form-data
          :response-format :json
          :keywords? true
          :handler on-success
          :error-handler
            (fn [{:keys [status response] :as x}]
              (if (= 200 status)
                ; cljs-ajax hates blank responses, but it's OK here
                (on-success)
                (if (and (= 409 status) (= "identifier-exists" (:error response)))
                  (on-duplicate-identifier identifier)
                  (on-failure))))})
      ; new dataset
      (POST (str config/endpoint "/projects/" project-id "/datasets/")
        {:body form-data
          :response-format :json
          :keywords? true
          :handler on-success
          :error-handler
            (fn [{:keys [status response] :as x}]
              (console/log (str x))
              (if (and (= 409 status) (= "identifier-exists" (:error response)))
                (on-duplicate-identifier identifier)
                (on-failure)))}))))

(defn dataset-load
  [{:keys [id identifier project-id on-success on-failure]
    :or {identifier nil}}]
  (assert (not (and id identifier)) "must have ID or identifier, not both")
  (assert (or id (and identifier project-id)) "must have project ID if identifier given")
  (let [path
        (if id
          (str config/endpoint "/datasets/" id "/shallow")
          (str config/endpoint "/projects/" project-id "/datasets/by-identifier/" identifier "/shallow"))]
    (GET path
      {:headers {:accept "application/json"}
        :keywords? true
        :format :json
        :response-format :json
        :handler on-success
        :error-handler
          (fn [{:keys [status response]}]
            (console/error "load of datset from" path " failed: " status " - " response)
            (on-failure
              (if id
                (str "dataset with ID " id " not found")
                (str "dataset with identifier '" identifier "' not found" ))))})))

(defn dataset-load-data
  [{:keys [id project-id identifier on-success on-failure]
    :or {identifier nil}}]
  (assert (not (and id identifier)) "must have ID or identifier, not both")
  (assert (or id (and identifier project-id)) "must have project ID if identifier given")
  (let [path
        (if id
          (str config/endpoint "/datasets/" id)
          (str config/endpoint "/projects/" project-id "/datasets/by-identifier/" identifier))]
    (GET path
      {:response-format :raw
        :handler on-success
        :error-handler
          (fn [{:keys [status response]}]
            (console/error "load of datset data from" path " failed: " status " - " response)
            (on-failure
              (if id
                (str "dataset with ID " id " not found")
                (str "dataset with identifier '" identifier "' not found" ))))})))


(defn dataset-acl-load
  [{:keys [id on-success on-failure]}]
  (assert id "missing dataset ID")
  (GET (str config/endpoint "/datasets/" id "/acl" )
    {:response-format :json
      :keywords? true
      :handler
        (fn [acl]
          (on-success (parse-acl acl)))
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to load dataset acl for id " id
            " status " status " response: " response)
          (on-failure))}))

(defn dataset-acl-save
  [{:keys [id acl on-success on-failure]}]
  (assert id "missing dataset ID")
  (assert acl "missing acl")
  (PUT (str config/endpoint "/datasets/" id "/acl")
    {:params acl
      :format :json
      :handler on-success
      :error-handler
      (fn [{:keys [status response]}]
        (console/error
          "failed to save acl for dataset id " id
          " status " status " response " response)
        (on-failure))}))

(defn dataset-find-chart-ids
  "Find the IDs of the charts which depend on the dataset with the given ID"
  [{:keys [id on-success on-failure]}]
  (assert (number? id))
  (let [path (str config/endpoint "/datasets/" id "/charts")]
    (GET path
      {:response-format :json
        :format :json
        :handler on-success
        :error-handler #(on-failure)})))

(defn dataset-delete
  [{:keys [id on-success on-failure]}]
  (assert (number? id))
  (let [path (str config/endpoint "/datasets/" id)]
    (DELETE path
      {:handler on-success
        :error-handler on-failure})))

;;;;; DASHBOARDS

(defn dashboard-delete
  [{:keys [id on-success on-failure]}]
  (assert (number? id))
  (let [path (str config/endpoint "/dashboards/" id)]
    (DELETE path
      {:handler on-success
        :error-handler on-failure})))

(defn dashboard-load
  "load the dashboard with the specified id"
  [{:keys [id on-success on-failure]}]
  (assert (number? id) "id must be number")
  (GET (str config/endpoint "/dashboards/" id)
    {:format :json
      :response-format :json
      :keywords? true
      :handler on-success
      :error-handler
      (fn [{:keys [status response]}]
        (on-failure (str response)))}))

(defn dashboard-search
  [{:keys [project-id term sort-by limit offset descending? on-success on-failure with-user-acl?]
    :or {term "" sort-by :identifier limit 20 offset 0 descending? true}
    :as p}]
  (assert
    (some #(= sort-by %) [:identifier :description :content-type])
    (str "sort-by must be one of [:name :description :content-type], was " sort-by))
  (assert (not (nil? project-id)) "missing project id")
  (GET (str config/endpoint "/projects/" project-id "/dashboards/search")
    {:params
      {:query term
        :limit limit
        :offset offset
        :sort-by (name sort-by)
        :descending descending?
        :with-user-acl (true? with-user-acl?)}
      :format :json
      :response-format :json
      :keywords? true
      :handler (if with-user-acl? #(on-success (parse-search-response %)) on-success)
      :error-handler
        (fn [m]
          (console/error "dashboard search failed: " (str m))
          (on-failure))} ))

(defn dashboard-save
  "Save the given dashboard to the endpoint (new dashboard if no :id in dashboard,
    updating existing dashboard if :id provided).  on-success will be given the ID
  of the dashboard if save succeeds.  on-failure will be given the error from
  the server if one is available.  on-duplicate-name will be given the name of
  the dashboard which was in conflict."
  [{:keys [dashboard project-id on-success on-failure on-duplicate-name]}]
  (let [new? (nil? (:id dashboard))
        path
        (if new?
          (str config/endpoint "/projects/" project-id "/dashboards/")
          (str config/endpoint "/dashboards/" (:id dashboard)))]
    (assert (or (and new? (number? project-id)) (not new?))
      "project-id required if dashboard does not already exist")
    ((if new? POST PUT) path
      {:params
        (assoc
          (dissoc dashboard :charts)
          :chart-ids
          (map #(:id %) (:charts dashboard)))
        :format :json
        :response-format :json
        :keywords? true
        :handler (fn [r] (on-success (:id r)))
        :error-handler
        (fn [{:keys [status response]}]
          (if (= 409 status)
            (on-duplicate-name (:name dashboard))
            (on-failure (:error response))))})))

;;;;; PROJECTS
(defn project-search
  [{:keys [term with-user-acl? limit offset on-success on-failure]
    :or {with-user-acl? true limit 1000 offset 0}}]
  (GET (str config/endpoint "/projects/search")
    :response-format :json
    :keywords? true
    :params
      {:term term
        :with-user-acl with-user-acl?
        :limit limit
        :offset offset}
    :handler (if with-user-acl? #(on-success (parse-search-response %)) on-success)
    :error-handler
    (fn [{:keys [status response]}]
      (console/error "search for projects failed w/ term '" term "'")
      (on-failure))))

(defn project-acl-load
  [{:keys [id user-id on-success on-failure]}]
  (assert id "missing project ID")
  (assert (or (nil? user-id) (number? user-id)) "user id must be number if given")
  (GET (str config/endpoint "/projects/" id "/acl" )
    {:params (if user-id {:user-id user-id} {})
      :response-format :json
      :keywords? true
      :handler
        (fn [acl]
          (on-success (parse-acl acl)))
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to load project acl for id " id
            " status " status " response: " response)
          (on-failure))}))

(defn project-load
  "Load the project with the given ID, optionally providing the ID of the user for
  whom a partial ACL should be retrieved"
  [{:keys [id on-success on-failure acl-user-id]}]
  (assert id "missing project ID")
  (assert (or (nil? acl-user-id) (number? acl-user-id)) "acl user id must be number if given")
  (GET (str config/endpoint "/projects/" id )
    {:response-format :json
      :keywords? true
      :handler
      (fn [project]
        (if acl-user-id
          (project-acl-load
            {:id id
              :user-id acl-user-id
              :on-success #(on-success project %)
              :on-failure #(on-failure)})
          (on-success project)))
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to load project id " id " status " status " response: " response)
          (on-failure) )}))

(defn project-delete
  [{:keys [id on-success on-failure]}]
  (assert id "missing project ID")
  (DELETE (str config/endpoint "/projects/" id )
    {:format :raw
      :body ""
      :response-format :raw
      :keywords? true
      :handler on-success
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed to delete project id " id " status " status " response: " response)
          (on-failure) )}))

(defn project-save
  [{:keys [project on-success on-failure on-duplicate-name]}]
  (let [new? (nil? (:id project))]
    ((if new? POST PUT) (str config/endpoint "/projects/" (if (not new?) (:id project)))
      {:format :json
        :response-format :json
        :keywords? true
        :params project
        :handler #(on-success (:id %))
        :error-handler
          (fn [{:keys [status response] :as x}]
            (if (and (= 409 status) (= "name-exists" (:error response)))
              (on-duplicate-name (:name project))
              (on-failure)))})))



(defn project-acl-save
  [{:keys [id acl on-success on-failure]}]
  (assert id "missing project ID")
  (assert acl "missing acl")
  (PUT (str config/endpoint "/projects/" id "/acl")
    {:params acl
      :format :json
      :handler on-success
      :error-handler
      (fn [{:keys [status response]}]
        (console/error
          "failed to save acl for project id " id
          " status " status " response " response)
        (on-failure))}))


;;;;; RESOURCES

(defn resource-load
  [{:keys [chart-id path on-success on-failure]}]
  (assert chart-id "missing chart id")
  (assert (not (empty? path)) "missing path")

  ; NOTE: we omit the trailing slash on resources since our paths all begin
  ; with that since the "root" is under the chart itself
  (GET (str config/endpoint "/charts/" chart-id "/resources/" path)
    {:format :raw
      :response-format :raw
      :handler on-success
      :error-handler
        (fn [{:keys [status response]}]
          (console/error
            "failed resource load for chart id " chart-id
            " - path " path " - status " status " response: " (str response))
          (if (= 404 status)
            (on-failure (str "resource with name '" path "' not found"))
            (on-failure (str "http error " status))))}))

(defn resource-save
  [{:keys [chart-id path content on-success on-failure]}]
  (assert chart-id "missing chart-id")
  (assert (not (empty? path)) "missing resource path")
  (let [form-data (js/FormData.)]
    (.append form-data "data" content)
    (PUT (str config/endpoint "/charts/" chart-id "/resources/" path)
      {:body form-data
        :format :raw
        :response-format :raw
        :handler on-success
        :error-handler
          (fn [{:keys [status response]}]
            (console/error
              "failed resource save for chart id " chart-id
              " - path " path " - status " status " response: " (str response))
            (on-failure))})))


;;;;;; USERS

(defn user-load
  [id on-success on-failure]
  (assert (or (keyword? id) (number? id)))
  (let [id (if (keyword? id) (name id) id)
        path (str config/endpoint "/users/" id)]
    (GET path
         {:format :json
          :response-format :json
          :keywords? true
          :handler on-success
          :error-handler
            (fn [{:keys [status response]}]
              (console/error "failed to load user " id " - " status " - " response)
              (on-failure))})))

(defn user-search
  [{:keys [query limit offset on-success on-failure]
    :or {limit 1000 offset 0}}]
  (let [path (str config/endpoint "/users/search")]
    (GET path
      {:params
        {:query query
          :limit limit
          :offset offset}
        :format :json
        :response-format :json
        :handler on-success
        :keywords? true
        :error-handler
          (fn [{:keys [status response]}]
            (console/error (str "failed user search: " status " - " response))
            (on-failure))})))

(defn user-save
  [{:keys [id username] :as user}
    {:keys [on-success on-duplicate-username on-failure]}]
  (let [path (str config/endpoint "/users/" (when id id))]
    ((if id PUT POST) path
      {:params user
        :format :json
        :response-format :json
        :keywords? true
        :handler on-success
        :error-handler
          (fn [{:keys [status response]}]
            (if (= 409 status)
              (on-duplicate-username username)
              (do
                (console/error (str "failed to save user: " status " - " response))
                (on-failure))) )})))

(defn user-delete
  [{:keys [id on-success on-failure]}]
  (assert (number? id))
  (let [path (str config/endpoint "/users/" id)]
    (DELETE path
      {:handler on-success
        :error-handler
          (fn [{:keys [status response]}]
            (console/error (str "failed user deletion: " status " - " response))
            (on-failure))})))

(defn user-change-auth-source
  [{:keys [id]} email on-success on-failure]
  (assert (number? id))
  (let [path (str config/endpoint "/users/changeToAuth0")
        params {:userId id, :email email}]
    (POST path
      {:params params
       :format :json
       :response-format :json
       :keywords? true
       :handler on-success
       :error-handler (fn [{:keys [status response]}]
                        (console/error (str "failed changing user auth source: " status " - " response))
                        (on-failure status))})))
