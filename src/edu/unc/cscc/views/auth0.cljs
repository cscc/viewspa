(ns edu.unc.cscc.views.auth0
  (:require ["createAuth0Client"]
            [cljs.core.async :refer [>! alts! chan timeout go]]
            [cljs.core.async.interop :refer-macros [<p!]]
            [re-frame.core :refer [dispatch dispatch-sync]]
            [cljs.reader :refer [read-string]]
            [shodan.console :as console]

            [edu.unc.cscc.views.routes :as routes]
            [edu.unc.cscc.views.config :as config]
            [edu.unc.cscc.views.auth :as auth]))

(def createAuth0Client (.-createAuth0Client js/window))

(declare client)

(defn set-client! [value]
  (defonce client value))

(defn logout
  ([]
   (logout ""))
  ([query]
   (.logout ^js client #js {:returnTo (str js/window.location.origin config/homepage query)})))

(defn handle-error [err key]
  (console/error "Failed login at key:" key "-" (str err))
  (go
    (if (<p! (. client getTokenSilently))
      (logout "?loaderror=auth0-failure"))))

(defn ^:private query-includes? [str]
  (let [query js/window.location.search]
    (and (string? query)
         (.includes query str))))

(defn ^:private should-handle-callback? []
  (and (query-includes? "code=")
       (query-includes? "state=")))

(defn ^:private fetch-auth0-config [result-channel]
  (go
    (let [response (try
                     (<p! (js/fetch "/js/auth0-config.json"))
                     (catch js/Error err (handle-error err :fetch-auth0-config)))
          config (try
                   (<p! (. response json))
                   (catch js/Error err (handle-error err :parse-auth0-config)))]
      (>! result-channel config))))

(defn ^:private check-authentication [config result-channel]
  (let [is-authenticated (fn [^js client] (. client isAuthenticated))
        get-app-state (fn [^js result] (. result -appState))
        handle-redirect-callback (fn [^js client]
                                   (. client handleRedirectCallback))]
    (go
      (let [{:strs [domain clientId]} (js->clj config)
            config (clj->js {:domain domain, :client_id clientId})
            _ (try
                (set-client! (<p! (createAuth0Client config)))
                (catch js/Error err (handle-error err :create-client)))
            callback-result (when (should-handle-callback?)
                              (try
                                (<p! (handle-redirect-callback client))
                                (catch js/Error err
                                  (handle-error err :handle-callback))))
            page (some-> callback-result get-app-state read-string)
            _ (js/window.history.replaceState #js {}
                                              js/document.title
                                              js/window.location.pathname)
            authenticated? (try
                             (<p! (is-authenticated client))
                             (catch js/Error err
                               (handle-error err :check-authentication)))]
        (>! result-channel [page authenticated?])))))

(defn ^:private auth-error? []
  (and (query-includes? "error=")
       (query-includes? "error_description=")))

(defn ^:private check-auth-error
  "If there was an error, handle the error and return true, else return false."
  []
  (when (auth-error?)
    (let [search-params (->> js/window.location.search
                             (new js/URLSearchParams)
                             .entries
                             (map js->clj)
                             (into {}))
          {:strs [error error_description]} search-params
          message (if (not= error "access_denied")
                    (str "An unknown error occurred.")
                    (str "Access denied. An administrator must authorize you "
                         "for this app before you may use it."))
          message (str message
                       " Technical details follow: "
                       (pr-str (dissoc search-params "state")))]
      (js/window.history.replaceState #js {}
                                      js/document.title
                                      js/window.location.pathname)
      (handle-error #js {:message message} :check-auth-error)
      true)))

(defn login-with-redirect []
  (let [options #js {:redirect_uri (str js/window.location.origin config/homepage)
                     :appState (pr-str nil)}
        promise (.loginWithRedirect ^js client options)]
    (.then promise
           (constantly nil)
           (fn [err] (handle-error err :login)))))


(defn do-auth!
  "Do everything needed to handle authentication with Auth0. If the user is
  authenticated successfully, call the given function passing the JWT token to the server.
  If the user is not successfully authenticated, call the given function with no arguments (this does not indicate an error, simply that no auth0 query parameters are present)"
  [after-authenticated-successfully after-not-authenticated]
  (go
    (let [auth-error? (check-auth-error)
          response-channel (chan)
          _ (fetch-auth0-config response-channel)
          config (<! response-channel)
          check-auth-channel (chan)
          _ (check-authentication config check-auth-channel)
          [page authenticated?] (<! check-auth-channel)]
      (when-not auth-error?
        (if-not authenticated?
          (after-not-authenticated)
          (let [access-token (<p! (. client getTokenSilently))]
            (auth/auth0-login access-token after-authenticated-successfully #(handle-error (:status %) :server-error))))))))
