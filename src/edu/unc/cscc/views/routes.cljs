(ns edu.unc.cscc.views.routes
  (:require [re-frame.core :refer [dispatch
                                   dispatch-sync]]
            [secretary.core :as secretary :include-macros true]
            [shodan.console :as console]
            [goog.events :as events]
            [goog.history.EventType :as EventType]

            [edu.unc.cscc.views.config :as config]
            [edu.unc.cscc.views.utils.urls :as url-utils]
            [edu.unc.cscc.views.auth :as auth])
  (:import goog.History))

;;;;; route setup

(defonce ^:private h (History.))

(defn go!
  [route]
  (if (.startsWith route "#")
    (.setToken h (.substring route 1))
    (.setToken h route)))

;;;;; utility
(defn- parse-integer
  "Parses the given string as a number, returning the fallback value if parsing
  failed for any reason."
  [str fallback]
  (if (re-find #"^\d+$" str)
    (.parseInt js/window str 10)
    fallback))

;;;;; routes

(secretary/defroute main "/" []
  (go! "/projects"))

(secretary/defroute login "/login" []
  (dispatch [:set-view :login {:show-nav false}]))

(secretary/defroute logout "/logout" []
  (dispatch-sync [:set-view :limbo])
  (dispatch-sync [:check-auth0-logout])
  (dispatch-sync [:initialize])
  (go! "/login"))

(secretary/defroute about "/about" []
  (dispatch [:set-view :about]))

; dashboards
(secretary/defroute dashboards "/projects/:project-id/dashboards" [project-id]
  (let [project-id (parse-integer project-id :wtf)]
    (if (= :wtf project-id)
      (go! "/")
      (dispatch [:set-view :dashboards {:project-id project-id}]))))

(secretary/defroute dashboard "/projects/:project-id/dashboards/:id" [project-id id query-params]
  (let [id (if (= "new" id) :new (parse-integer id :wtf))
        project-id (parse-integer project-id :wtf)
        action (get-in query-params [:action] :view)]
    (if (or (= :wtf id) (= :wtf project-id))
      (go! "/")
      (dispatch
        [:set-view :dashboards
          {:show-nav true
            :grid-width 2
            :project-id project-id
            :id id
            :mode
              (if (or (= action "edit") (= :new id))
                :edit
                :view)}]))))

; chart listing
(secretary/defroute charts "/projects/:project-id/charts" [project-id]
  (let [project-id (parse-integer project-id :wtf)]
    (if (= :wtf project-id)
      (go! "/")
      (dispatch [:set-view :charts {:project-id project-id}]))))
; individual charts
(secretary/defroute chart "/projects/:project-id/charts/:id" [project-id id query-params]
  (let [id (if (= "new" id) :new (parse-integer id :wtf))
        project-id (parse-integer project-id :wtf)
        action (get-in query-params [:action] :view)]
    (if (or (= :wtf id) (= :wtf project-id))
      (go! "/")
      (dispatch
        [:set-view :charts
          {:project-id project-id
            :id id
            :mode
              (if (or (= action "edit") (= :new id))
                :edit
                (if (= action "edit-script")
                  :edit-script))
            :show-nav (not (= action "edit-script"))}]))))

; data set listing
(secretary/defroute datasets "/projects/:project-id/datasets" [project-id]
  (let [project-id (parse-integer project-id :wtf)]
    (if (= :wtf project-id)
      (go! "/")
      (dispatch [:set-view :datasets {:project-id project-id}]))))

; individual data sets
(secretary/defroute dataset "/projects/:project-id/datasets/:id" [project-id id]
  (let [id (if (= "new" id) :new (parse-integer id :wtf))
        project-id (parse-integer project-id :wtf)]
    (if (or (= :wtf id) (= :wtf project-id))
      (go! "/")
      (dispatch [:set-view :datasets {:project-id project-id :id id}]))))

; project management
(secretary/defroute projects "/projects" []
  (dispatch [:set-view :projects]))

(secretary/defroute project "/projects/:id" [id]
  (let [id (if (= "new" id) :new (parse-integer id :wtf))]
    (if (= :wtf id)
      (go! (projects))
      (dispatch [:set-view :projects {:id id}]))))

; user management
(secretary/defroute users "/users/" []
  (dispatch [:set-view :users]))
(secretary/defroute user "/users/:id" [id]
  (let [id (if (= "new" id) :new (parse-integer id :wtf))]
    (if (= :wtf id)
      (go! (users))
      (dispatch-sync [:set-view :users {:id id}]))))

(defn initialize!
  []
  (secretary/set-config! :prefix "#")
  (events/listen h EventType/NAVIGATE
    (fn [e]
      (-> e .-token secretary/dispatch!)))
  (.setEnabled h true))
