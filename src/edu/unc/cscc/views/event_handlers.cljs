(ns edu.unc.cscc.views.event-handlers
  (:require [secretary.core :as secretary :include-macros true]
            [re-frame.core :refer [reg-event-db
                                   reg-event-fx
                                   reg-fx
                                    path
                                    register-sub
                                    dispatch
                                    dispatch-sync
                                    subscribe]]
            [shodan.console :as console :include-macros true]

            [edu.unc.cscc.views.auth :as auth]
            [edu.unc.cscc.views.auth0 :as auth0]
            [edu.unc.cscc.views.config :as config]
            [edu.unc.cscc.views.remote-api :as remote]
            [edu.unc.cscc.views.initial-state :refer [initial-state]]
            [edu.unc.cscc.views.routes :as routes]))



(defn initialize-state!
  [db _]
    initial-state)

(reg-event-db
  :initialize
  initialize-state!)

(reg-event-db
  :set
  (fn [db [_ path v]]
    (assoc-in db path v)))

(reg-event-db
  :unset
  (fn [db [_ path]]
    (assert (and (vector? path) (not (empty? path))))
    (update-in db (vec (drop-last path)) dissoc (last path)) ))


; view handlers

(reg-event-db
  :set-view-state
  (fn [db [_ state]]
    (update-in db [:view]
      assoc :state state)))

(reg-event-db
  :set-view
  (fn [db [_ name state]]
    (update-in db [:view]
      assoc
      :name name
      :state (if state state {}))))

(reg-event-db
 :set-load-error
 (fn [db [_ error]]
   (assoc db :load-error error)))

; user state

(defn user-load-success-handler!
  [user]
  (dispatch-sync [:set-user user])
  (routes/go! (routes/project)))

(defn user-load-failure-handler!
  [{:keys [status status-text]}]
  (if (= status 401)
    (dispatch [:set-view-state :rejected])
    (do
      (dispatch [:set-view-state :failure])
      (console/info "Login failed: " status status-text))))

(reg-event-db
  :user-login
  (fn
    [app-state [_ username password credential-source]]
    (dispatch [:set-view :login :in-progress])
    (auth/login
      username password credential-source
      user-load-success-handler!
      user-load-failure-handler!)
    app-state))

(reg-fx
 :auth0-logout
 (fn [_]
   (auth0/logout)))

;; Ideally this would produce effects that log users out, but until we upgrade to
;; re-frame 1.10, there's no way to guarentee order of events. So we just end up
;; calling a bunch of dispatch-syncs
(reg-event-fx
 :check-auth0-logout
 (fn [{:keys [db]} _]
   (if (= config/auth0-credential-source-id (get-in db [:user :entity :credential-source-id]))
     {:auth0-logout true}
     {})))

(reg-event-db
  :set-jwt-token
  (fn [db [_ token]]
    (assoc-in db [:user :token] token)))


(reg-event-db
  :set-user
  (fn [db [_ user]]
    (assoc-in db [:user :entity] user)))
