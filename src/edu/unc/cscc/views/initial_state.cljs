(ns edu.unc.cscc.views.initial-state)

(def ^:export initial-state
  {
    :view {
      :name :login
      :state
      {; :project-id if in project
      }
    }

    :user {
      :entity nil
      :token nil
    }

    :users {
      :entities {} ; by ID
      :search {}
    }
    :dashboards
    {
      :entities {} ; by ID
      :search {}
    }
    :charts {
      :entities {} ; by ID
      :search {
        ; term {:results [] :acls {id {:permission granted?}}}
      }
    }
    :resource {
      :by-chart {} ; by chart ID
      :entities {} ; by res ID
    }
    :datasets {
      :batch-data {} ; by batch load params
      :data {} ; by dataset ID
      :entities {} ; by ID
      :search {}
    }
    :projects {
      :entities {}
      :search {}
      :acl {}
    }

    :credential-sources nil ; by identifier

  })
