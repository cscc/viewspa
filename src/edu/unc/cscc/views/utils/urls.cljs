(ns edu.unc.cscc.views.utils.urls
  (:require [clojure.string :as string]))

(defn redirect!
  "Redirects to the passed page"
  [loc]
  (set! (.-location js/window) loc))

(defn parse-search-params
  "Parses URL search params into a map of keyword -> string key value pairs.
  Expects the same format as returned by (-> js/window .-location .-search).
  This does not do any url decoding"
  [search-params]
  (->>
   (-> search-params
       (subs 1)
       (string/split "&"))
   (map #(string/split %1 "="))
   (reduce
    (fn [result [k v]]
      (assoc result (keyword k) v))
    {})))

(defn map-to-search-params
  "Converts a map to search params. See also `parse-search-params`.
  This does not do any url encoding"
  [search-params]
  (if (empty? search-params)
    ""
    (->> search-params
         (map (fn [[k v]] (str k "=" v)))
         (string/join "&")
         (str "?"))))

(defn set-search-params!
  "Sets the current url to the search params represented by the object passed in"
  [search-params]
  (let [origin (.. js/window -location -origin)
        path (.. js/window -location -pathname)
        hash (.. js/window -location -hash)
        search-params-string (map-to-search-params search-params)]
    (.. js/window -history (replaceState nil "" (str origin path search-params-string hash)))))
