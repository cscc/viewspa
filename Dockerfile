FROM clojure:lein

COPY . /opt/src/
WORKDIR /opt/src

RUN /usr/local/bin/lein fig -- -b dev
#RUN /usr/local/bin/lein cljsbuild once dev

FROM nginx:alpine

COPY ./resources/public/ /usr/share/nginx/html/
