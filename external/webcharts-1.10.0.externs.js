/**********************************************************************
 * Extern for webCharts
 * Generated by http://jmmk.github.io/javascript-externs-generator
 * with subsequent modifications by hand.
 **********************************************************************/
var webCharts = {
  "version": {},
  "dataOps": {
    "getValType": function () {},
    "lengthenRaw": function () {},
    "naturalSorter": function () {},
    "summarize": function () {}
  },
  "objects": {
    "chart": {
      "raw_data": {},
      "config": {},
      "destroy": function() {}
    },
    "table": {
      "raw_data": {},
      "config": {},
      "destroy": function() {}
    },
    "controls": {
      "changeOption": function () {},
      "checkRequired": function () {},
      "controlUpdate": function () {},
      "init": function () {},
      "layout": function () {},
      "makeControlItem": function () {},
      "makeBtnGroupControl": function () {},
      "makeCheckboxControl": function () {},
      "makeDropdownControl": function () {},
      "makeListControl": function () {},
      "makeNumberControl": function () {},
      "makeRadioControl": function () {},
      "makeSubsetterControl": function () {},
      "makeTextControl": function () {},
      "stringAccessor": function () {},
      "destroy": function() {}
    }
  },
  "createChart": function () {},
  "createControls": function () {},
  "createTable": function () {},
  "multiply": function () {}
};
/**********************************************************************
 * End Generated Extern for webCharts
/**********************************************************************/
